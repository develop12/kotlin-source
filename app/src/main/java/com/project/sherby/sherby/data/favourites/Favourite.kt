package com.project.sherby.sherby.data.favourites

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Favourite : Serializable {

    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("price")
    @Expose
    var price: String? = null
    @SerializedName("date")
    @Expose
    var date: String? = null
    @SerializedName("location")
    @Expose
    var location: String? = null
    @SerializedName("images")
    @Expose
    var images: String? = null


}