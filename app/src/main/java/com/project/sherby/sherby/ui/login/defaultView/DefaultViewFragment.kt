package com.project.sherby.sherby.ui.login.defaultView

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.login.login.LoginFragment
import com.project.sherby.sherby.ui.login.signup.SignUpFragment
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.nested_fragment_defaultview.*

/**
 * Created by ${Shubham} on 6/4/2018.
 */

class DefaultViewFragment : BaseFragment(), View.OnClickListener {
    override fun onClick(v: View?) {

        when (v) {
            tvSignUpButton -> {
                activity!!.login_included_layout.visibility = View.INVISIBLE
                activity!!.included_second_layout.visibility = View.VISIBLE
                replaceFragment(SignUpFragment(), false, R.id.frame_login)
            }
            tvLogin -> {
                activity!!.login_included_layout.visibility = View.INVISIBLE
                activity!!.included_second_layout.visibility = View.VISIBLE
                replaceFragment(LoginFragment(), false, R.id.frame_login)
            }
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.nested_fragment_defaultview, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        clickListeners()
    }

    private fun initViews() {

    }

    private fun clickListeners() {
        tvSignUpButton.setOnClickListener(this)
        tvLogin.setOnClickListener(this)
    }
}


