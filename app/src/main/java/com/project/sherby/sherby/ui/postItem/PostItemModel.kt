package com.project.sherby.sherby.ui.postItem

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.MyViewModel

class PostItemModel : MyViewModel() {
    var response = MutableLiveData<RegisterUserPojo>()

    fun postItem(title: String,
                 description: String,
                 price: String,
                 city: String,
                 country: String,
                 mobile: String,
                 cat: String,
                 specification: String,
                 userId: String,
                 images: ArrayList<String?>,
                 levelId: String,
                 listingStatus: String) {

        isLoading.value = true
        PostItemRepository.postItem({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, title, description, price, city,country, mobile, cat, specification, userId, images, levelId, listingStatus)
    }

}