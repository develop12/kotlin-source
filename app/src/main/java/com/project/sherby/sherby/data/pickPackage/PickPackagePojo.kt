package com.project.sherby.sherby.data.pickPackage

import java.io.Serializable

class PickPackagePojo : Serializable {

    var success: Int? = null
    var message: String? = null
    var packages: List<Package>? = null


}