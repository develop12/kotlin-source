package com.project.sherby.sherby.application

import Preferences
import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp
import java.util.*

class Application : Application() {

    var mContext: Context? = null

    companion object AppContext {
        lateinit var instance: com.project.sherby.sherby.application.Application
        fun getContext(): Context {
            return instance
        }
    }

    init {
        instance = this
    }


    override fun onCreate() {
        super.onCreate()

//        val token = Utils.customPrefs(this).getValue(Constants.TOKEN, "")
        FirebaseApp.initializeApp(this)
        Preferences.initPreferences(this)
        mContext = applicationContext


        val locale = Locale("fr")
        Locale.setDefault(locale)
        val config = this.resources.configuration
        config.setLocale(locale)
        this.createConfigurationContext(config)
        this.resources.updateConfiguration(config, this.resources.displayMetrics);
    }
}
