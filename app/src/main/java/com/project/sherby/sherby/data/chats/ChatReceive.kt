package com.project.sherby.sherby.data.chats

/**
 * Created by ${Shubham} on 6/21/2018.
 */
data class ChatReceive(var msg: String, var sender: String)