package com.project.sherby.sherby.activity

import Preferences
import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterDrawerItems
import com.project.sherby.sherby.data.home.Category
import com.project.sherby.sherby.ui.adsDescription.ViewAdFragment
import com.project.sherby.sherby.ui.chats.MainChatsFragment
import com.project.sherby.sherby.ui.filters.FiltersActivity
import com.project.sherby.sherby.ui.home.HomeFragment
import com.project.sherby.sherby.ui.login.LoginActivity
import com.project.sherby.sherby.ui.notifications.NotificationFragment
import com.project.sherby.sherby.ui.postItem.PostItemFragment
import com.project.sherby.sherby.ui.userProfile.UserProfileFragment
import com.project.sherby.sherby.utils.BaseActivity
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.app_bar_drawer.*
import kotlinx.android.synthetic.main.item_notifications.*
import kotlinx.android.synthetic.main.launcher_activity.*
import kotlinx.android.synthetic.main.map_dialog.*
import java.io.File
import java.util.*


class LauncherActivity : BaseActivity(), View.OnClickListener, RecyclerViewClickListener, OnMapReadyCallback {
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!

//        mMap.setOnCameraIdleListener {
//
//            val midLatLng = mMap.cameraPosition.target
//            getAddressFromLatLong(midLatLng)
//
//        }
    }

    override fun onClick(view: View, position: Int, url: String) {
        args.putString(Constants.CATEGORY_ID, items[position].id.toString())
//        args.putString(Constants.LOCATION, city)
        loadInitialFragment()
        drawer_layout.closeMenu()
//        drawer_layout.closeDrawer(Gravity.START)
        Toast.makeText(this, position.toString(), Toast.LENGTH_SHORT).show()

    }

    private lateinit var mMap: GoogleMap
    private val DEFAULT_ZOOM = 15
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var args = Bundle()
    private var items: ArrayList<Category> = ArrayList()
    private val CAMERA_REQUEST = 0
    private val GALLERY_PICTURE = 1
    private var selectedImagePath: String? = null
    private var mViewModel: CategoryFetchModel? = null
    private var city: String = ""
    private var country: String = ""
    private var completeAddress: String = ""
    private val imagesUriArrayList: ArrayList<Uri> = ArrayList()

    override fun onClick(v: View?) {
        when (v) {
            ivHome -> {
//                args.putString(Constants.LOCATION, completeAddress)
                val homeFragment = HomeFragment()
                homeFragment.arguments = args
                tvSellYourStuff.visibility = View.VISIBLE
                rlTabLayout.visibility = View.VISIBLE
                rlToolbar.visibility = View.VISIBLE
                tvItemLocation.visibility = View.VISIBLE
                replaceFragment(homeFragment, R.id.fragment_container)
                changeDrawableTintColor(this, ivHome)
            }
            ivNotifications -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(this@LauncherActivity, LoginActivity::class.java))
                    finish()
                } else {
                    tvItemLocation.visibility = View.GONE
                    tvSellYourStuff.visibility = View.INVISIBLE
                    rlTabLayout.visibility = View.VISIBLE
                    rlToolbar.visibility = View.GONE
                    replaceFragment(NotificationFragment(), R.id.fragment_container)
                    changeDrawableTintColor(this, ivNotifications)
                }
            }
            ivSellStuff -> {
                tvItemLocation.visibility = View.GONE
                tvSellYourStuff.visibility = View.INVISIBLE
                rlToolbar.visibility = View.GONE
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(this@LauncherActivity, LoginActivity::class.java))
                    finish()
                } else {
                    replaceFragment(PostItemFragment(), R.id.fragment_container)
                }

                changeDrawableTintColor(this, ivSellStuff)

            }
            ivChats -> {
                tvItemLocation.visibility = View.GONE
                rlToolbar.visibility = View.GONE
                tvSellYourStuff.visibility = View.INVISIBLE
                rlTabLayout.visibility = View.VISIBLE
                if (Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    replaceFragment(MainChatsFragment(), R.id.fragment_container)
                    changeDrawableTintColor(this, ivChats)
                } else
                    tvSellYourStuff.performClick()
            }
            ivProfile -> {
                removeFragments()
                tvItemLocation.visibility = View.GONE
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(this@LauncherActivity, LoginActivity::class.java))
                    finish()
                } else {
                    tvSellYourStuff.visibility = View.INVISIBLE
                    replaceFragment(UserProfileFragment(), R.id.fragment_container)
                    changeDrawableTintColor(this, ivProfile)
                }
            }
            ivFilter -> {
                tvSellYourStuff.visibility = View.INVISIBLE
                val intent = Intent(this@LauncherActivity, FiltersActivity::class.java)
                startActivity(intent)
            }

            tvDrawerLayout -> {
                drawer_layout.openMenu()
            }

            tvSellYourStuff -> {
                ivSellStuff.performClick()
            }

            tvInvite -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Join me on Sherby -- link to apk")
                startActivity(Intent.createChooser(shareIntent, "Condividi con..."))
            }

            tvItemLocation -> {
                if (canGetLocation())
                    openMapFragment()
                else
                    startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

            else -> { // Note the block
                print("x is neither 1 nor 2")
            }
        }
    }

    override fun getID(): Int {
        return R.layout.launcher_activity
    }

    override fun iniView(savedInstanceState: Bundle?) {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.launcher_activity)
        args.putString(Constants.CATEGORY_ID, "0")

        mViewModel = ViewModelProviders.of(this).get(CategoryFetchModel::class.java)
        attachObservers()
        mViewModel?.fetchCategories()

        setData()
        checkLocationPermission()

        clickListeners()
        searchFunctionality()

        if(intent.hasExtra("fromNotif"))
        {
        ivNotifications.performClick()
        }


        loadInitialFragment()

    }

    private fun setData() {
        if (canGetLocation()) {
            if (checkLocationPermission()) {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
                    getAddressFromLatLong(LatLng(location?.latitude!!, location?.longitude))
                }
            }

        } else {
            Toast.makeText(this, "blobber", Toast.LENGTH_SHORT).show()
        }
        tvSellYourStuff.bringToFront()
        tvItemLocation.bringToFront()
        ivHome.setImageResource(R.mipmap.house)
        ivNotifications.setImageResource(R.mipmap.notification)
        ivChats.setImageResource(R.mipmap.chat_buddle)
        ivSellStuff.setImageResource(R.mipmap.camera)

    }

    private fun searchFunctionality() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                filter(charSequence.toString())
            }

            override fun afterTextChanged(editable: Editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString())
            }
        })
    }

    private fun filter(string: String) {
        var homeFragment: HomeFragment? = null

        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        if (fragment is HomeFragment) {
            homeFragment = fragment
        }

        if (homeFragment != null) {
            homeFragment.homeItemAdapter!!.filter.filter(string)
        }
    }


    private fun attachObservers() {
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it.let {
                if (it != null) {
                    items = it as ArrayList<Category>
                    setUpDrawer()
                }
                //showSnackBar(message)
            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {

            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {

        })
    }
    //IneXT-HELLO78$09151986


    private fun setUpDrawer() {
        rvDrawerItems.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvDrawerItems.adapter = AdapterDrawerItems(items, this, this)
        rvDrawerItems.isNestedScrollingEnabled = true
    }


    private fun loadInitialFragment() {
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        val homeFragment = HomeFragment()
        homeFragment.arguments = args
        replaceFragment(homeFragment, R.id.fragment_container)
        changeDrawableTintColor(this, ivHome)
    }

    private fun clickListeners() {

        tvItemLocation.setOnClickListener(this)
        ivHome.setOnClickListener(this)
        ivNotifications.setOnClickListener(this)
        ivSellStuff.setOnClickListener(this)
        ivChats.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        ivFilter.setOnClickListener(this)
        tvDrawerLayout.setOnClickListener(this)
        tvSellYourStuff.setOnClickListener(this)
        tvInvite.setOnClickListener(this)

    }


    override fun onBackPressed() {
        val fragmentManager1 = this.supportFragmentManager
        val fragment: Fragment = fragmentManager1.findFragmentById(R.id.fragment_container)

        if (fragment is ViewAdFragment) {
            rlTabLayout.visibility = View.VISIBLE
            rlToolbar.visibility = View.VISIBLE
//            tvSellYourStuff.visibility = View.VISIBLE
//            tvItemLocation.visibility = View.VISIBLE
        }

        if (fragment is HomeFragment) {
            tvSellYourStuff.visibility = View.VISIBLE
        }

        if (fragment is UserProfileFragment) {

            rlToolbar.visibility = View.VISIBLE
            tvItemLocation.visibility = View.GONE
            tvSellYourStuff.visibility = View.GONE

        }

//        if(fragment is SettingsFragment){
//            replaceFragment(UserProfileFragment(),R.id.fragment_container)
//        }

        super.onBackPressed()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        selectedImagePath = null

        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {

            var f = File(Environment.getExternalStorageDirectory()
                    .toString())
            for (temp in f.listFiles()!!) {
                if (temp.name == "temp.jpg") {
                    f = temp
                    break
                }
            }

            if (!f.exists()) {
                Toast.makeText(baseContext,
                        "Error while capturing image", Toast.LENGTH_LONG)
                        .show()
                return
            }
        }

        else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            imagesUriArrayList.clear()

            for (i in 0 until data?.clipData!!.itemCount) {
                imagesUriArrayList.add(data.clipData?.getItemAt(i)?.uri!!)
            }
//            adapter = DataAdapter(this@MainActivity, imagesUriArrayList)
//            imageresultRecycletview.setAdapter(adapter)
//            adapter.notifyDataSetChanged()
        }
    }

    private fun getAddressFromLatLong(midLatLng: LatLng) {

        val geocoder = Geocoder(this, Locale.getDefault())
        val addresses: MutableList<Address>? = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1)
        addresses!![0].getAddressLine(1)
        city = addresses[0].adminArea
        country = addresses[0].countryName
        completeAddress = addresses[0].getAddressLine(0)
        tvItemLocation.text = city

    }

    private fun openMapFragment() {
        val dialog = Dialog(this, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.map_dialog)
        dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.show()
        MapsInitializer.initialize(this)
        val mMapView: MapView? = dialog.findViewById(R.id.mapView)
        mMapView!!.onCreate(dialog.onSaveInstanceState())
        mMapView.getMapAsync(this)
        mMapView.onResume()// needed to get the map to display immediately
        dialog.tvSetLocation.setOnClickListener {
            getAddressFromLatLong(mMap.cameraPosition.target)

            args.putString(Constants.LOCATION, city)
            loadInitialFragment()
            dialog.dismiss()
        }
        fetchCurrenLocation()
    }


    @SuppressLint("MissingPermission")
    private fun fetchCurrenLocation() {
        fusedLocationClient!!.lastLocation
                .addOnSuccessListener { location: Location? ->
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location!!.latitude,
                            location.longitude), DEFAULT_ZOOM.toFloat()))
                }
    }
}
