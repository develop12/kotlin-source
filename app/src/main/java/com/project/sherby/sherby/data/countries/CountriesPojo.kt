package com.project.sherby.sherby.data.countries

import java.io.Serializable

class CountriesPojo : Serializable {

    var success: Int? = null
    var message: String? = null
    var countries: List<Country>? = null


}