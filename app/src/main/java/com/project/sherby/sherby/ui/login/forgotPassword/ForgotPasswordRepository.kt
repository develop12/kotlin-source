package com.project.sherby.sherby.ui.login.forgotPassword

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by ${Shubham} on 7/25/2018.
 */
object ForgotPasswordRepository {

    private val webService = ApiHelper.createService()

    fun forgotPassword(successHandler: (RegisterUserPojo) -> Unit, failureHandler: (String) -> Unit, user_login: String) {
        webService.forgotPassword(user_login).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    successHandler(it)

                }
                if (response?.code() == 422) {
                    response.errorBody()?.let {
                        val error = ApiHelper.handleAuthenticationError(response.errorBody()!!)
                        failureHandler(error)
                    }

                } else {
                    response?.errorBody()?.let {
                        val error = ApiHelper.handleApiError(response.errorBody()!!)
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }

}