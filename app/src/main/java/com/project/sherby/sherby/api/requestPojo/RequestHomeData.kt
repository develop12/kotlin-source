package com.project.sherby.sherby.api.requestPojo

import com.google.gson.annotations.SerializedName

/**
 * Created by ${Shubham} on 6/1/2018.
 */
data class RequestHomeData(

        @field:SerializedName("password")
        var catId: Int? = null,

        @field:SerializedName("role")
        var currentPage: Int? = null

)