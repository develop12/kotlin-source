package com.project.sherby.sherby.data.notifications


import com.google.gson.annotations.SerializedName


class NotificationDatum {

    @SerializedName("bid_amount")
    var bidAmount: String? = null
    @SerializedName("id")
    var id: Long? = null
    @SerializedName("listImage")
    var listImage: String? = null
    @SerializedName("listid")
    var listid: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("sender")
    var sender: String? = null
    @SerializedName("senderid")
    var senderid: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("userImage")
    var userImage: String? = null

}
