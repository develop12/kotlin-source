package com.project.sherby.sherby.adapter

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import kotlinx.android.synthetic.main.item_selected_images.view.*

/**
 * Created by ${Shubham} on 8/7/2018.
 */
class AdapterPostedImages(var items: ArrayList<Uri>, var context: Context) :
        RecyclerView.Adapter<AdapterPostedImages.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return AdapterPostedImages.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_selected_images, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AdapterPostedImages.ViewHolder, position: Int) {
        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.ic_person)
        Glide.with(context).load(items[position]).into(holder.ivImage)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivImage = view.ivPostImage!!
    }

}