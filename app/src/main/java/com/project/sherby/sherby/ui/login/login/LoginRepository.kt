package com.project.sherby.sherby.ui.login.login

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.login.LoginUserPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by android on 19/2/18.
 *
 * Data Provider for the application
 */
object LoginRepository {
    private val webService = ApiHelper.createService()

    fun getData(successHandler: (LoginUserPojo) -> Unit, failureHandler: (String) -> Unit,
                user_login: String, user_pass: String, token: String, type: String) {
        webService.login(user_login, user_pass, token, type).enqueue(object : Callback<LoginUserPojo> {
            override fun onResponse(call: Call<LoginUserPojo>?, response: Response<LoginUserPojo>?) {
                response?.body()?.let {
                    successHandler(it)

                }
                if (response?.code() == 422) {
                    response.errorBody()?.let {
                        val error = ApiHelper.handleAuthenticationError(response.errorBody()!!)
                        failureHandler(error)
                    }

                } else {
                    response?.errorBody()?.let {
                        val error = ApiHelper.handleApiError(response.errorBody()!!)
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<LoginUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }
}