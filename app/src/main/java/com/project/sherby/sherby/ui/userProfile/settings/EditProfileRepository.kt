package com.project.sherby.sherby.ui.userProfile.settings

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by android on 5/4/18.
 */
object EditProfileRepository {
    private val webService = ApiHelper.createService()


    fun editProfileData(successHandler: (RegisterUserPojo) -> Unit, failureHandler: (String) -> Unit, params: Map<String, RequestBody>) {
        webService.editProfile(params).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)

                    }
                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }


    fun changePassword(successHandler: (RegisterUserPojo) -> Unit, failureHandler: (String) -> Unit,
                       userId: String,
                       oldPassword: String,
                       newPassword: String) {
        webService.changePassword(userId, oldPassword, newPassword).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }
}