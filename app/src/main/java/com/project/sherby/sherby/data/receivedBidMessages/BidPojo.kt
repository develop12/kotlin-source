package com.project.sherby.sherby.data.receivedBidMessages

import com.google.gson.annotations.SerializedName


class BidPojo {

    @SerializedName("message")
    var message: String? = null
    @SerializedName("response")
    var response: Response? = null
    @SerializedName("success")
    var success: Int? = null

}
