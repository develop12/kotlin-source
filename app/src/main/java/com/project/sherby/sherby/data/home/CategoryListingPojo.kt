package com.project.sherby.sherby.data.home

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class CategoryListingPojo : Serializable {

    @SerializedName("Categories")
    @Expose
    var categories: List<Category>? = null

}