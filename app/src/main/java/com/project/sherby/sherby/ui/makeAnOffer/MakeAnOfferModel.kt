package com.project.sherby.sherby.ui.makeAnOffer

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by ${Shubham} on 7/23/2018.
 */
class MakeAnOfferModel : MyViewModel() {
    var response = MutableLiveData<RegisterUserPojo>()


    fun fetchAdsInfo(postId: String,
                     bidAmount: String,
                     fromUserId: String,
                     messageContent: String) {
        isLoading.value = true
        MakeAnOfferRepository.makeAnOffer({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, postId, bidAmount, fromUserId, messageContent)
    }

}