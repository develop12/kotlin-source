package com.project.sherby.sherby.ui.notifications

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.notifications.NotificationPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by ${Shubham} on 7/24/2018.
 */
class NotificationViewModel : MyViewModel() {

    var response = MutableLiveData<NotificationPojo>()
    fun fetchData(userId: String) {

        isLoading.value = true
        NotificationsRepository.fetchNotifications({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, userId)
    }

}