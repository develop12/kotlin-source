package com.project.sherby.sherby.activity

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.countries.CountriesPojo
import com.project.sherby.sherby.data.home.Category
import com.project.sherby.sherby.data.pickPackage.PickPackagePojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object FetchCategoryRepository {

    private val webService = ApiHelper.createService()


    fun fetchCategories(successHandler: (List<Category>) -> Unit, failureHandler: (String) -> Unit) {
        webService.fetchCategory().enqueue(object : Callback<List<Category>> {
            override fun onResponse(call: Call<List<Category>>?, response: Response<List<Category>>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<List<Category>>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }


    fun fetchPackages(successHandler: (PickPackagePojo) -> Unit, failureHandler: (String) -> Unit) {
        webService.pickPackage().enqueue(object : Callback<PickPackagePojo> {
            override fun onResponse(call: Call<PickPackagePojo>?, response: Response<PickPackagePojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<PickPackagePojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }

    fun fetchCountries(successHandler: (CountriesPojo) -> Unit, failureHandler: (String) -> Unit) {
        webService.fetchCountries().enqueue(object : Callback<CountriesPojo> {
            override fun onResponse(call: Call<CountriesPojo>?, response: Response<CountriesPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<CountriesPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }
}