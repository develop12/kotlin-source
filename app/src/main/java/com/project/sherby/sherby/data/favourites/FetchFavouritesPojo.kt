package com.project.sherby.sherby.data.favourites

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


import java.io.Serializable

class FetchFavouritesPojo : Serializable {

    @SerializedName("success")
    @Expose
    var success: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("favourites")
    @Expose
    var favourites: List<Favourite>? = null

}