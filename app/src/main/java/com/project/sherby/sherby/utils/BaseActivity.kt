package com.project.sherby.sherby.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.project.sherby.sherby.R
import kotlinx.android.synthetic.main.activity_filters.*
import kotlinx.android.synthetic.main.app_bar_drawer.*
import java.util.*


/**
 * Created by android on 8/11/17.
 * Base activity for all activities
 */
@SuppressLint("Registered")
open abstract class BaseActivity : AppCompatActivity() {
    protected var mDoubleBackToExitPressedOnce = false

    private val PERMISSION_REQUEST = 121

    protected val TAG: String = javaClass.simpleName

    private var mProgressDialog: Dialog? = null


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        setContentView(getID())
        iniView(savedInstanceState)
        super.onCreate(savedInstanceState, persistentState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
    }


    abstract fun getID(): Int
    abstract fun iniView(savedInstanceState: Bundle?)


    fun showSnackBar(message: String, content: View) {
        this.let {
            Snackbar.make(content, message, Snackbar.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // pass activity's  result to the fragments
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        fragment?.onActivityResult(requestCode, resultCode, data)

    }

    fun canGetLocation(): Boolean {
        return isLocationEnabled(this) // application context
    }

    private fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0;
        val locationProviders: String

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
            }
            locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_MODE)
            !TextUtils.isEmpty(locationProviders);
        }
    }

    fun checkLocationPermission(): Boolean {
        val permission1 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
        val permission2 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION)
        val permission3 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
        val permission4 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
        val permission5 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CALL_PHONE)
        val result = permission1 == PackageManager.PERMISSION_GRANTED
                && permission2 == PackageManager.PERMISSION_GRANTED
                && permission3 == PackageManager.PERMISSION_GRANTED
                && permission4 == PackageManager.PERMISSION_GRANTED
                && permission5 == PackageManager.PERMISSION_GRANTED
        if (!result) {
            requestPermission()
        }
        return result
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        fragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun replaceFragment(fragment: Fragment, id: Int) {
        val tag: String = fragment::class.java.simpleName
        val transaction = supportFragmentManager?.beginTransaction()
        transaction?.setCustomAnimations(R.anim.fade_in,
                R.anim.fade_out)
        transaction?.replace(id, fragment, tag)
                ?.commitAllowingStateLoss()
    }


    /**
     * Add fragment with or without addToBackStack
     *
     * @param fragment       which needs to be attached
     * @param addToBackStack is fragment needed to backstack
     */


    fun addFragment(fragment: Fragment, addToBackStack: Boolean, id: Int) {
        val tag = fragment.javaClass.simpleName
        val fragmentManager = this.supportFragmentManager
        val fragmentOldObject = fragmentManager?.findFragmentByTag(tag)
        val transaction = fragmentManager?.beginTransaction()
        transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out, R.anim.anim_in_reverse, R.anim.anim_out_reverse)
        if (fragmentOldObject != null) {
            fragmentManager.popBackStackImmediate(tag, 0)
        } else {
            if (addToBackStack) {
                transaction?.addToBackStack(tag)
            }
            transaction?.add(id, fragment, tag)
                    ?.commit()
        }
    }


    /**
     **************** handle API response code and manage action accordingly *******
     */


    //will use later

    fun changeDrawableTintColor(activity: Activity, ivImageView: ImageView) {
        activity.ivHome.setColorFilter(ContextCompat.getColor(activity, R.color.primary_text_default_material_light), android.graphics.PorterDuff.Mode.MULTIPLY)
        activity.ivNotifications.setColorFilter(ContextCompat.getColor(activity, R.color.primary_text_default_material_light), android.graphics.PorterDuff.Mode.MULTIPLY)
        activity.ivSellStuff.setColorFilter(ContextCompat.getColor(activity, R.color.primary_text_default_material_light), android.graphics.PorterDuff.Mode.MULTIPLY)
        activity.ivChats.setColorFilter(ContextCompat.getColor(activity, R.color.primary_text_default_material_light), android.graphics.PorterDuff.Mode.MULTIPLY)
        activity.ivProfile.setColorFilter(ContextCompat.getColor(activity, R.color.primary_text_default_material_light), android.graphics.PorterDuff.Mode.MULTIPLY)

        ivImageView.setColorFilter(ContextCompat.getColor(this, R.color.themeColor))
    }

    fun sortByColor(activity: Activity, tvTextView: TextView) {
        activity.tvNewestFirst.setTextColor(Color.BLACK)
        activity.tvNewestFirst.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        activity.tvClosestFirst.setTextColor(Color.BLACK)
        activity.tvClosestFirst.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        activity.tvPriceLowToHigh.setTextColor(Color.BLACK)
        activity.tvPriceLowToHigh.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        activity.tvPriceHighToLow.setTextColor(Color.BLACK)
        activity.tvPriceHighToLow.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

        tvTextView.setTextColor(resources.getColor(R.color.themeColor))
        tvTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check, 0)
    }

    fun postedWithin(activity: Activity, tvTextView: TextView) {
        activity.tvLast24Hour.setTextColor(Color.BLACK)
        activity.tvLast24Hour.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        activity.tvLast7Days.setTextColor(Color.BLACK)
        activity.tvLast7Days.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        activity.tvLast30Days.setTextColor(Color.BLACK)
        activity.tvLast30Days.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        activity.tvAllListings.setTextColor(Color.BLACK)
        activity.tvAllListings.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

        tvTextView.setTextColor(resources.getColor(R.color.themeColor))
        tvTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check, 0)
    }


    /*************************Extension functions for image loading **********************************/


    fun ImageView.loadImage(url: String) {
        Glide.with(this)
                .load(url)
                .into(this)

    }

    protected fun setProfileImage(imagePath: String?, imageView_profile: ImageView, progressBar: ProgressBar?) {
        Glide.with(this)
                .load(imagePath)
                .into(imageView_profile)
    }

    fun showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = Dialog(this, android.R.style.Theme_Translucent)
            mProgressDialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            mProgressDialog!!.setContentView(R.layout.loader_half__layout)
            mProgressDialog!!.setCancelable(false)

        }
        mProgressDialog!!.show()
    }

    fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog!!.dismiss()
        }
    }

    fun showLoading(show: Boolean?) {
        if (show!!) showProgress() else hideProgress()
    }

    /**
     ***************** when permission is diabled then show user a dialog
     * force to enable permission from setting
     */

    fun permissionDenied() {
        val builder = android.support.v7.app.AlertDialog.Builder(this)

        builder.setMessage(getString(R.string.permission_denied))

        val positiveText = getString(android.R.string.ok)
        builder.setPositiveButton(positiveText
        ) { dialog, which ->
            enablePermission()
        }

        val negativeText = getString(android.R.string.cancel)
        builder.setNegativeButton(negativeText
        ) { dialog, which ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        // display dialog
        dialog.show()
    }

    /**
     ************** start app detail activity to enable disabled permissions ***********
     */

    private fun enablePermission() {
        val packageName = packageName

        try {
            //Open the specific App Info page:
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.data = Uri.parse("package:$packageName")
            startActivityForResult(intent, PERMISSION_REQUEST)

        } catch (e: ActivityNotFoundException) {
            //e.printStackTrace();
            //Open the generic Apps page:
            val intent = Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
            startActivityForResult(intent, PERMISSION_REQUEST)

        }

    }

    /**
     ************ clear images stored in folder bestyme of map snaps *******
     */


    fun checkForStoragePermission(): Boolean {

        val permission1 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE)
        val result = permission1 == PackageManager.PERMISSION_GRANTED
        if (!result) {
            requestStoragePermission()
        }
        return result

    }

    //use later
    private fun requestStoragePermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE), Constants.PERMISSION_REQUEST_CODE)
                // permissionDenied()
                return false
            }
        }
        return true
    }


    fun View.visible(boolean: Boolean): Int {
        if (boolean)
            return View.VISIBLE
        else
            return View.INVISIBLE
    }

    /**
     * This method will request permission
     */
    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE),
                Constants.LOCATION_PERMISSION_REQUEST_CODE)
    }
//
//    FragmentManager fm = getActivity().getSupportFragmentManager();
//    for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
//        fm.popBackStack();
//    }

    fun removeFragments() {
        val fm = this.supportFragmentManager
        for(i in 0 until fm.backStackEntryCount)
        {
            fm.popBackStack()
        }
    }

    override fun onResume() {

//        val locale = Locale("ru")
//        Locale.setDefault(locale)
//        val config = baseContext.resources.configuration
//        config.locale = locale
//        baseContext.resources.updateConfiguration(config,
//                baseContext.resources.displayMetrics)




        super.onResume()

    }
}