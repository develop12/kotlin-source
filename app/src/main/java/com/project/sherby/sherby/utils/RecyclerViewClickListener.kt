package com.project.sherby.sherby.utils

import android.view.View

interface RecyclerViewClickListener {
    fun onClick(view: View, position: Int, url: String)
}