package com.project.sherby.sherby.ui.adsDescription

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import com.project.sherby.sherby.data.singlelisting.SingleListingPojo
import com.project.sherby.sherby.ui.userProfile.UserDetailRepository
import com.project.sherby.sherby.utils.MyViewModel

class AdsDescriptionModel : MyViewModel() {

    var response = MutableLiveData<SingleListingPojo>()
    var responseAddToFavourites = MutableLiveData<AddFavouritesPojo>()

    fun fetchAdsInfo(postId: String, userId: String) {

        isLoading.value = true
        AdsInformationRepository.getTodoData({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, postId, userId)

    }

    fun addFavourites(postId: String, userId: String) {

        isLoading.value = true
        AdsInformationRepository.addFavourites({
            responseAddToFavourites.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, postId, userId)
    }


    var responseDelete = MutableLiveData<AddFavouritesPojo>()
    fun deletePost(postId: String) {
        isLoading.value = true
        AdsInformationRepository.deletePost({
            responseDelete.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, postId)
    }

    var responseUnpublish = MutableLiveData<AddFavouritesPojo>()
    fun unpublish(p_id: String, userId: String) {
        isLoading.value = true
        AdsInformationRepository.unpublish({
            responseUnpublish.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, p_id,userId)
    }



    var responseUnsold = MutableLiveData<AddFavouritesPojo>()
    fun unsold(p_id: String, userId: String, status: String) {
        isLoading.value = true
        AdsInformationRepository.unsold({
            responseUnsold.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, p_id,userId,status)
    }


}