package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.pickPackage.Package
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_pack_layout.view.*

/**
 * Created by ${Shubham} on 8/3/2018.
 */
class AdapterPickYourPack(var items: ArrayList<Package>, private val context: Context
                          , private val clickListener: RecyclerViewClickListener) :
        RecyclerView.Adapter<AdapterPickYourPack.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPickYourPack.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pack_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AdapterPickYourPack.ViewHolder, position: Int) {

        holder.tvTotalNoOfPictures.text = items[position].images_number
        holder.tvDays.text = items[position].activation_period
        holder.tvBumpsUp.text = items[position].description
        holder.tvPriceOfPack.text = items[position].price
        holder.tvPackId.text = items[position].name
        holder.init(clickListener)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvTotalNoOfPictures = view.tvTotalNoOfPictures!!
        val tvDays = view.tvDays!!
        var tvBumpsUp = view.tvBumpsUp!!
        var tvPriceOfPack = view.tvPriceOfPack!!
        var tvPackId = view.tvPackId!!

        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {
                click.onClick(itemView, adapterPosition, tvDays.text.toString())
            }
        }
    }

}