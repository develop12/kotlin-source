package com.project.sherby.sherby.ui.notifications

import Preferences
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterNotifications
import com.project.sherby.sherby.data.notifications.NotificationDatum
import com.project.sherby.sherby.data.notifications.NotificationPojo
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.chats.chatsScreen.ActivityChatScreen
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.fragment_notifications.*

/**
 * Created by ${Shubham} on 5/29/2018.
 */

class NotificationFragment : BaseFragment(), View.OnClickListener, RecyclerViewClickListener {
    override fun onClick(view: View, position: Int, url: String) {

        val args = Bundle()
        args.putString(Constants.POST_ID, items[position].listid)
//            var chatFragment=ActivityChatScreen()
//            chatFragment.arguments
        val intent = Intent(activity, ActivityChatScreen::class.java)
        intent.putExtras(args)
        startActivity(intent)

    }

    private var items: ArrayList<NotificationDatum> = ArrayList()
    private var mViewModel: NotificationViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(NotificationViewModel::class.java)
        attachObservers()

    }

    private fun attachObservers() {
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it.let {
                val message = "${it!!.message}"

                if (it.success!! == 1) {
                    if (Integer.valueOf(it.totalNotification) > 0) {
                        setData(it)
                    }
                } else
                    showSnackBar(message)
            }

        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })

    }

    private fun setData(it: NotificationPojo) {

        items = it.notificationData as ArrayList<NotificationDatum>

        rvNotifications.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rvNotifications.adapter = AdapterNotifications(items, requireContext(), this)

    }

    override fun onClick(v: View?) {

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        mViewModel?.fetchData(Preferences.prefs?.getString(Constants.USER_ID, "0")!!)

    }

}