package com.project.sherby.sherby.data.newSingleList


import com.google.gson.annotations.SerializedName


class SingleListData {

    @SerializedName("message")
    var message: String? = null
    @SerializedName("Post_data")
    var postData: PostData? = null
    @SerializedName("success")
    var success: Int? = null

}
