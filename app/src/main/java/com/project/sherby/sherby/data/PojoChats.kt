package com.project.sherby.sherby.data

/**
 * Created by ${Shubham} on 5/30/2018.
 */
data class PojoChats(var image: Int, var userName: String, var itemToSell: String, var time: String)