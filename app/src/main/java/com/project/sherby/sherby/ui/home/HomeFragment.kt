package com.project.sherby.sherby.ui.home

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.HomeItemAdapter
import com.project.sherby.sherby.data.home.Listing
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.adsDescription.ViewAdFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.EndlessScrollListener
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.app_bar_drawer.*
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * Created by ${Shubham} on 5/28/2018.
 */
class HomeFragment : BaseFragment(), RecyclerViewClickListener {

    private val args = Bundle()
    private var cat_id: String = "0"
    private var address: String = "0"
    private var mViewModel: HomeFragmentModel? = null
    private var startVal: Int = 1
    var homeItemAdapter: HomeItemAdapter? = null
    private var items: ArrayList<Listing> = ArrayList()

    override fun onClick(view: View, position: Int, url: String) {

        activity!!.tvItemLocation.visibility = View.GONE
        args.putString(Constants.PRODUCT_ID, url)
        val viewAdFragment = ViewAdFragment()
        viewAdFragment.arguments = args
        activity!!.tvSellYourStuff.visibility = View.INVISIBLE
        addFragment(viewAdFragment, true, R.id.fragment_container)
        activity!!.rlTabLayout.visibility = View.GONE
        activity!!.rlToolbar.visibility = View.GONE

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(HomeFragmentModel::class.java)
        attachObservers()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBundledArguments()
        //     getHomeItemsData()

    }

    private fun getBundledArguments() {


        val bundle = arguments

        initViews()
        if (bundle != null) {
            if (bundle.containsKey(Constants.LOCATION)) {
                address = bundle.getString(Constants.LOCATION)

            }
            cat_id = bundle.getString(Constants.CATEGORY_ID)
            getHomeItemsData()
        }


    }

    private fun initViews() {

        homeItemAdapter = HomeItemAdapter(items, requireContext(), this)
        rvHomeItems.layoutManager = StaggeredGridLayoutManager(2, 1)
        rvHomeItems.adapter = homeItemAdapter
        /* val snapHelper:SnapHelper=PagerSnapHelper()
         snapHelper.attachToRecyclerView(rvHomeItems)*/

    }

    private fun getHomeItemsData() {
        if (address != "0") {
            mViewModel?.fetchLocationData(address, cat_id, startVal.toString())
        } else {
            mViewModel?.fetchData(/*address,*/ cat_id, startVal.toString())
        }
    }

    private fun setUpRecyclerView() {


        activity!!.runOnUiThread { homeItemAdapter!!.notifyDataSetChanged() }
        val scrollListener = EndlessScrollListener(object : EndlessScrollListener.RefreshList {
            override fun onRefresh(pageNumber: Int) {
                if (startVal <= 3) {
                    startVal += 1
                    getHomeItemsData()
                }
            }
        })
        rvHomeItems.addOnScrollListener(scrollListener)


    }


    private fun attachObservers() {


        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                if (it.success == 1) {
                    items.addAll(it.listing as ArrayList<Listing>)
                    setUpRecyclerView()
                }
            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })




        mViewModel?.responseLocationFilter?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                if (it.success == 1) {
                    items.addAll(it.listing as ArrayList<Listing>)
                    setUpRecyclerView()
                } else
                    showSnackBar(it.message!!)
            }
        })

        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })
    }


}