package com.project.sherby.sherby.ui.login.forgotPassword

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by ${Shubham} on 7/25/2018.
 */
class ForgotPasswordViewModel : MyViewModel() {

    var response = MutableLiveData<RegisterUserPojo>()

    fun authenticate(user_login: String) {
        isLoading.value = true
        ForgotPasswordRepository.forgotPassword({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, user_login)
    }

}