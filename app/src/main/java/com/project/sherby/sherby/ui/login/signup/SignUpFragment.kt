package com.project.sherby.sherby.ui.login.signup

import Preferences
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.login.login.LoginFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.Validations
import com.project.sherby.sherby.utils.saveValue
import kotlinx.android.synthetic.main.fragment_otp.view.*
import kotlinx.android.synthetic.main.included_top_signup.*
import kotlinx.android.synthetic.main.nested_fragment_signup.*

/**
 * Created by ${Shubham} on 6/4/2018.
 */
class SignUpFragment : BaseFragment(), View.OnClickListener {

    private var mViewModel: SignUpModel? = null
    private var otp: String = "0"
    override fun onClick(v: View?) {

        when (v) {
            tvSignUp -> {
                if (Validations.isEmpty(etUserName)) {
                    if (Validations.isEmpty(etPasswordSignUp)) {
                        if (Validations.isEmpty(etMobile)) {
                            if (Validations.isValidPassword(etPasswordSignUp)) {
                                mViewModel?.verifyUser(etMobile.text.toString())
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(SignUpModel::class.java)
        attachObservers()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.nested_fragment_signup, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        clickListeners()
    }

    private fun initViews() {
        activity!!.tvSignUpButton.setBackgroundResource(R.drawable.login_top_left_filled_background)
        activity!!.tvSignUpButton.setTextColor(resources.getColor(android.R.color.white))

        activity!!.tvLogin.setBackgroundResource(R.drawable.login_top_right_background)
        activity!!.tvLogin.setTextColor(resources.getColor(R.color.themeColor))
    }

    private fun clickListeners() {
        tvSignUp.setOnClickListener(this)
    }


    private fun attachObservers() {
        //profile created response
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = "${it.message}"
                if (it.success!! == 1) {
                    if (it.userId != null) {
                        Preferences.prefs?.saveValue(Constants.IS_LOGGED_IN, true)
                        Preferences.prefs?.saveValue(Constants.USER_ID, it.userId)
                    }
                    replaceFragment(LoginFragment(), true, R.id.frame_login)
                } else {
                    showSnackBar(message)
                    showOtpDialog()
                }
            }
        })

        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })

        //otp

        mViewModel?.responseVerify?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = it.message
                if (message != null) {
                    showSnackBar(message)
                }

                if (it.success == 1) {
                    otp = it.verification_code!!
                    showSnackBar("OTP has been sent to phone number")
                    showOtpDialog()
                } else {
                    showOtpDialog()
                }
            }
        })

        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })

    }

    private fun showOtpDialog() {

        val dialogBuilder = AlertDialog.Builder(requireContext())
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.fragment_otp, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        dialogView.tvSubmitOtp.setOnClickListener {

            hitOtpApi(dialogView.etEnterOtp.text.toString())
            b.dismiss()
            dialogBuilder.setCancelable(false)
        }
        b.show()

    }

    private fun hitOtpApi(otp: String) {

        mViewModel?.registerUser(etUserName.text.toString().trim(),
                etPasswordSignUp.text.toString().trim(),
                etMobile.text.toString().trim(),
                otp)
    }
}