package com.project.sherby.sherby.ui.postItem

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object PostItemRepository {

    private val webService = ApiHelper.createService()
    fun postItem(successHandler: (RegisterUserPojo) -> Unit,
                 failureHandler: (String) -> Unit,
                 title: String,
                 description: String,
                 price: String,
                 city: String,
                 country: String,
                 mobile: String,
                 cat: String,
                 specification: String,
                 userId: String,
                 images: ArrayList<String?>,
                 levelId: String,
                 listingStatus: String) {

        webService.postItem(title, description, price, city,
                country, mobile, cat, specification, userId, images, levelId, listingStatus).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }
}