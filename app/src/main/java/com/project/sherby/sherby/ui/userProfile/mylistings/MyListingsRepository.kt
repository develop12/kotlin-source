package com.project.sherby.sherby.ui.userProfile.mylistings

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.favourites.FetchFavouritesPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by ${Shubham} on 7/5/2018.
 */
object MyListingsRepository {

    private val webservice = ApiHelper.createService()

    fun getMyListings(successHandler: (FetchFavouritesPojo) -> Unit, failureHandler: (String) -> Unit, userId: String) {
        webservice.fetchMyListings(userId).enqueue(object : Callback<FetchFavouritesPojo> {
            override fun onResponse(call: Call<FetchFavouritesPojo>?, response: Response<FetchFavouritesPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }


            override fun onFailure(call: Call<FetchFavouritesPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }


        })
    }
}


