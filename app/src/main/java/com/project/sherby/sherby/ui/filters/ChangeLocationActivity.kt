package com.project.sherby.sherby.ui.filters

import android.location.Location
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_change_location.*

class ChangeLocationActivity : BaseActivity(), OnMapReadyCallback, com.google.android.gms.location.LocationListener {
    override fun onLocationChanged(p0: Location?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val mDefaultLocation = LatLng(-33.8523341, 151.2106085)
    private val DEFAULT_ZOOM = 15
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun getID(): Int {
        return R.layout.activity_change_location
    }

    override fun iniView(savedInstanceState: Bundle?) {
    }

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_location)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fetchCurrentLocation()

        clickListeners()
    }

    private fun clickListeners() {
        tvSaveLocation.setOnClickListener {
            onBackPressed()
        }
    }

    private fun fetchCurrentLocation() {
        if (checkLocationPermission()) {
            fusedLocationClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        //                        mMap.addMarker(MarkerOptions().position(LatLng(location!!.latitude,location.longitude)).title("Current location"))
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location!!.latitude, location.longitude), DEFAULT_ZOOM.toFloat()))
                    }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnCameraIdleListener {

            val midLatLng = mMap.cameraPosition.target
            Toast.makeText(this@ChangeLocationActivity, midLatLng.latitude.toString(), Toast.LENGTH_SHORT).show()

        }
    }
}
