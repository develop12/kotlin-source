package com.project.sherby.sherby.ui.rateReview

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object RateReviewRepository {

    private val webService = ApiHelper.createService()

    fun submitReview(successHandler: (AddFavouritesPojo) ->
    Unit, failureHandler: (String) -> Unit, userId: String,
                     postId: String,
                     rating: String,
                     review: String) {
        webService.rateReview(userId, postId, rating, review).enqueue(object : Callback<AddFavouritesPojo> {
            override fun onResponse(call: Call<AddFavouritesPojo>?, response: Response<AddFavouritesPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }

            override fun onFailure(call: Call<AddFavouritesPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }

        })
    }


}
