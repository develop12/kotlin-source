package com.project.sherby.sherby.ui.report

import Preferences
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterReportUser
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.adsDescription.ViewAdFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.app_bar_drawer.*
import kotlinx.android.synthetic.main.fragment_report_user.*

/**
 * Created by ${Shubham} on 7/18/2018.
 */
class ReportPostFragment : BaseFragment(), View.OnClickListener, RecyclerViewClickListener {

    private var mViewModel: ReportPostModel? = null
    private var p_id: String = "0"
    private var reason: String = "0"

    private var message: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(ReportPostModel::class.java)
        attachObservers()
    }

    private fun attachObservers() {
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {

                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    setData()

                } else
                    showSnackBar(message)

            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })

    }

    private fun setData() {
        val args = Bundle()
        args.putString(Constants.PRODUCT_ID, p_id)
        val viewAdFragment = ViewAdFragment()
        viewAdFragment.arguments = args
        activity!!.tvSellYourStuff.visibility = View.INVISIBLE
        addFragment(viewAdFragment, true, R.id.fragment_container)
    }

    override fun onClick(view: View, position: Int, url: String) {
        reason = items[position]
    }

    val items: ArrayList<String> = ArrayList()

    override fun onClick(v: View?) {
        when (v) {
            tvSubmitReport -> {
                mViewModel?.fetchData(Preferences.prefs?.getString(Constants.USER_ID, "0")!!,
                        p_id, reason, etReason.text.toString())
            }
            tvAdsDetails -> {
                setData()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_report_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        getBundledArguments()
        clickListeners()
    }

    private fun clickListeners() {
        tvSubmitReport.setOnClickListener(this)
        tvAdsDetails.setOnClickListener(this)
    }

    private fun setUpRecyclerView() {

        items.add("comportement offensant")
        items.add("Escroc")
        items.add("MIA à la rencontre")
        items.add("Comportement suspect")
        items.add("Inactif")
        items.add("Vente d'objets interdits")
        items.add("Spammer")
        items.add("Articles de contrefaçon")
        items.add("Autre")

        rvListReasons.layoutManager = StaggeredGridLayoutManager(1, 1)
        rvListReasons.adapter = AdapterReportUser(items, requireContext(), this)

    }

    private fun getBundledArguments() {
        val bundle = arguments

        if (bundle != null) {
            p_id = bundle.getString(Constants.POST_ID)
        }
    }
}