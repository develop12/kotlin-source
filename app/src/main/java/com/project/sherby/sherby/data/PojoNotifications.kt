package com.project.sherby.sherby.data

/**
 * Created by ${Shubham} on 5/29/2018.
 */
data class PojoNotifications(var image: Int, var notificationType: String, var checkOutTheirProfile: String)