package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.chatHistory.Chat_list
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_chats.view.*


class AdapterChats(private val items: ArrayList<Chat_list>, private val context: Context
                   , private val clickListener: RecyclerViewClickListener)
    : RecyclerView.Adapter<AdapterChats.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName.text = items[position].user_name
        Glide.with(context).load(items[position].user_image).into(holder.ivImage)
        holder.tvItemToSell.text = items[position].list_name
        holder.tvTime.text = items[position].time
        holder.init(clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_chats, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvName = view.tvChatName!!
        val ivImage = view.ivUserImage!!
        var tvItemToSell = view.tvItemToSell!!
        var tvTime = view.tvTime!!

        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {
                click.onClick(itemView, adapterPosition, tvName.text.toString())
            }
        }
    }
}