package com.project.sherby.sherby.data.notifications

import com.google.gson.annotations.SerializedName


class NotificationPojo {

    @SerializedName("message")
    var message: String? = null
    @SerializedName("notification_data")
    var notificationData: List<NotificationDatum>? = null
    @SerializedName("success")
    var success: Int? = null
    @SerializedName("total_notification")
    var totalNotification: String? = null

}
