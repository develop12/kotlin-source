package com.project.sherby.sherby.ui.login.signup


import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.getOtp.GetOtpPojo
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by android on 19/2/18.
 * model class for login
 */
class SignUpModel : MyViewModel() {

    var response = MutableLiveData<RegisterUserPojo>()
    var responseVerify = MutableLiveData<GetOtpPojo>()

    fun registerUser(user_login: String,
                     user_pass: String,
                     user_phone: String,
                     otp: String) {


        isLoading.value = true
        SignUpRepository.registerUser({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, user_login, user_pass, user_phone, otp)


    }

    fun verifyUser(user_phone: String) {
        isLoading.value = true
        SignUpRepository.verifyOtp({
            responseVerify.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, user_phone)

    }


}