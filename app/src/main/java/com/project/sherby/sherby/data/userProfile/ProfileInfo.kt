package com.project.sherby.sherby.data.userProfile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ProfileInfo {

    var userName: String? = null
    var userImage: String? = null
    var phone: String? = null
    var listing: List<Listing>? = null
    var favourite: List<Favourite>? = null

    @SerializedName("sold")
    @Expose
    var sold: List<Sold>? = null

}