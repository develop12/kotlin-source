package com.project.sherby.sherby.data.receivedMessages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Chat implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("message_title")
    @Expose
    public String message_title;
    @SerializedName("message_content")
    @Expose
    public String message_content;
    @SerializedName("sender_id")
    @Expose
    public String sender_id;
    @SerializedName("sender_name")
    @Expose
    public String sender_name;
    @SerializedName("reciever_id")
    @Expose
    public String reciever_id;
    @SerializedName("reciever_name")
    @Expose
    public String reciever_name;
    @SerializedName("time")
    @Expose
    public String time;


}