package com.project.sherby.sherby.data.singlelisting

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable
import android.media.Rating



class Post_data : Serializable {

    @SerializedName("postId")
    @Expose
    var postId: String? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("specification")
    @Expose
    var specification: String? = null

    @SerializedName("images")
    @Expose
    var images: List<String>? = null

    @SerializedName("cat")
    @Expose
    var cat: Any? = null

    @SerializedName("sub_cat")
    @Expose
    var sub_cat: Any? = null

    @SerializedName("price")
    @Expose
    var price: String? = null

    @SerializedName("views")
    @Expose
    var views: String? = null

    @SerializedName("userName")
    @Expose
    var userName: String? = null

    @SerializedName("userImage")
    @Expose
    var userImage: String? = null

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("location")
    @Expose
    var location: String? = null

    @SerializedName("mobile")
    @Expose
    var mobile: String? = null

    @SerializedName("latitude")
    @Expose
    var latitude: String? = null

    @SerializedName("longitude")
    @Expose
    var longitude: String? = null

    //"inFav": false
    @SerializedName("inFav")
    @Expose
    var inFav: Boolean? = null

    //authorId
    @SerializedName("authorId")
    @Expose
    var authorId: String? = null

    @SerializedName("bid_placed")
    @Expose
    var bid_placed: Boolean? = false

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("sold_status")
    @Expose
    var sold_status: String? = null

    @SerializedName("rating")
    @Expose
    var rating: List<com.project.sherby.sherby.data.singlelisting.Rating>? = null


}