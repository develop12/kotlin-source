package com.project.sherby.sherby.ui.chats

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.other.BaseFragment
import kotlinx.android.synthetic.main.fragment_chat_screen.*

/**
 * Created by ${Shubham} on 5/30/2018.
 */
class MainChatsFragment : BaseFragment(), View.OnClickListener {


    override fun onClick(v: View?) {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_screen, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTabs()
    }

    private fun setupTabs() {


        replaceChatFragment(FragmentChats(), true)
        tabChats.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> replaceChatFragment(FragmentChats(), true)
                    1 -> replaceChatFragment(FragmentChats(), true)
                    2 -> replaceChatFragment(FragmentChats(), true)
                    3 -> replaceChatFragment(FragmentChats(), true)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

}