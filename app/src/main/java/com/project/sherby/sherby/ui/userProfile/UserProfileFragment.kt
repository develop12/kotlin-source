package com.project.sherby.sherby.ui.userProfile

import Preferences
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterFavourites
import com.project.sherby.sherby.adapter.AdapterListings
import com.project.sherby.sherby.adapter.AdapterSold
import com.project.sherby.sherby.data.userProfile.Favourite
import com.project.sherby.sherby.data.userProfile.GetUserProfilePojo
import com.project.sherby.sherby.data.userProfile.Listing
import com.project.sherby.sherby.data.userProfile.Sold
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.adsDescription.ViewAdFragment
import com.project.sherby.sherby.ui.userProfile.settings.SettingsFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import com.project.sherby.sherby.utils.saveValue
import kotlinx.android.synthetic.main.app_bar_drawer.*
import kotlinx.android.synthetic.main.fragment_user_profile.*

/**
 * Created by ${Shubham} on 5/29/2018.
 */

class UserProfileFragment : BaseFragment(), View.OnClickListener, RecyclerViewClickListener {


    override fun onClick(view: View, position: Int, url: String) {
        if (url == "f") {
            args.putString(Constants.PRODUCT_ID, favourite[position].id.toString())
        } else {
            args.putString(Constants.PRODUCT_ID, listings[position].id.toString())
        }
        val viewAdFragment = ViewAdFragment()
        viewAdFragment.arguments = args
        activity!!.tvSellYourStuff.visibility = View.INVISIBLE
        addFragment(viewAdFragment, true, R.id.fragment_container)
        activity!!.rlTabLayout.visibility = View.GONE
        activity!!.rlToolbar.visibility = View.GONE

    }

    private var args = Bundle()

    private var mViewModel: UserProfileModel? = null

    //val homeFragment = FavouritesListFragment()

    var favourite: ArrayList<Favourite> = ArrayList()
    var sold: ArrayList<Sold> = ArrayList()
    var listings: ArrayList<Listing> = ArrayList()

    override fun onClick(v: View?) {

        when (v) {
            ivSettings -> {
                addFragment(SettingsFragment(), true, R.id.fragment_container)
            }

            tvMyListings -> {
                rvFavourites.layoutManager = StaggeredGridLayoutManager(2, 1)
                rvFavourites.adapter = AdapterListings(listings, requireContext(), this)

//                replaceFragment(MyListingsFragment(), true, R.id.rvFavourites)
                tvMyListings.setBackgroundColor(resources.getColor(R.color.themeColor))
                tvMyListings.setTextColor(Color.WHITE)

                tvFavourites.setBackgroundColor(Color.WHITE)
                tvFavourites.setTextColor(resources.getColor(R.color.themeColor))

                tvSold.setBackgroundColor(Color.WHITE)
                tvSold.setTextColor(resources.getColor(R.color.themeColor))
            }

            tvFavourites -> {
//                replaceFragment(FavouritesListFragment(), true, R.id.rvFavourites)

                rvFavourites.layoutManager = StaggeredGridLayoutManager(2, 1)
                rvFavourites.adapter = AdapterFavourites(favourite, requireContext(), this)

                tvMyListings.setBackgroundColor(Color.WHITE)
                tvMyListings.setTextColor(resources.getColor(R.color.themeColor))

                tvFavourites.setBackgroundColor(resources.getColor(R.color.themeColor))
                tvFavourites.setTextColor(Color.WHITE)


                tvSold.setBackgroundColor(Color.WHITE)
                tvSold.setTextColor(resources.getColor(R.color.themeColor))

            }

            tvSold -> {

                rvFavourites.layoutManager = StaggeredGridLayoutManager(2, 1)
                rvFavourites.adapter = AdapterSold(sold, requireContext(), this)

                tvFavourites.setBackgroundColor(Color.WHITE)
                tvFavourites.setTextColor(resources.getColor(R.color.themeColor))

                tvMyListings.setBackgroundColor(Color.WHITE)
                tvMyListings.setTextColor(resources.getColor(R.color.themeColor))


                tvSold.setBackgroundColor(resources.getColor(R.color.themeColor))
                tvSold.setTextColor(Color.WHITE)

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.tvSellYourStuff?.visibility = View.GONE
        activity?.tvItemLocation?.visibility = View.GONE
        mViewModel = ViewModelProviders.of(this).get(UserProfileModel::class.java)
        attachObservers()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        clickListeners()
    }

    private fun clickListeners() {

        tvSold.setOnClickListener(this)
        ivSettings.setOnClickListener(this)
        tvMyListings.setOnClickListener(this)
        tvFavourites.setOnClickListener(this)

    }

    private fun initViews() {
        args.putBoolean(Constants.FAVOURITES, true)
        activity!!.rlToolbar.visibility = View.GONE
//        homeFragment.arguments = args

        if (!Preferences.prefs?.getString(Constants.USER_ID, "0").isNullOrBlank() &&
                !Preferences.prefs?.getString(Constants.USER_ID, "0").isNullOrEmpty() &&
                !Preferences.prefs?.getString(Constants.USER_ID, "0").equals("0")
        ) {
            mViewModel!!.userDetails(Preferences.prefs?.getString(Constants.USER_ID, "0")!!)
        }

    }

    private fun attachObservers() {

        mViewModel?.response?.observe(this, Observer {
            it?.let {
                if (it.success == 1) {
                    setData(it)
                }
            }
        })

        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })

    }

    private fun setData(it: GetUserProfilePojo) {
        tvUsername.text = it.profileInfo!!.userName
        Glide.with(this).load(it.profileInfo!!.userImage!!)
                .apply(RequestOptions.bitmapTransform(CircleCrop()))
                .into(ivUserProfile)

        Preferences.prefs!!.saveValue(Constants.PHONE, it.profileInfo!!.phone.toString())

        favourite = it.profileInfo!!.favourite as ArrayList<Favourite>
        sold = it.profileInfo?.sold as ArrayList<Sold>
        listings = it.profileInfo!!.listing as ArrayList<Listing>

//        Preferences?.prefs.saveValue(Constants.USE)

        setUpTabs()
    }

    private fun setUpTabs() {
        rvFavourites.layoutManager = StaggeredGridLayoutManager(2, 1)
        rvFavourites.adapter = AdapterListings(listings, requireContext(), this)

    }
}