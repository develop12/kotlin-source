package com.project.sherby.sherby.data.receivedBidMessages

import com.google.gson.annotations.SerializedName

class Response {

    @SerializedName("bidamount")
    var bidamount: String? = null
    @SerializedName("chat")
    var chat: List<Chat>? = null
    @SerializedName("featured_image")
    var featuredImage: String? = null
    @SerializedName("id")
    var id: Long? = null
    @SerializedName("title")
    var title: String? = null

}
