package com.project.sherby.sherby.ui.rateReview

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import com.project.sherby.sherby.utils.MyViewModel

class RateReviewModel : MyViewModel() {

    var response = MutableLiveData<AddFavouritesPojo>()
    fun ratePost(userId: String,
                  postId: String,
                  rating: String,
                  review: String) {

        isLoading.value = true
        RateReviewRepository.submitReview({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, userId, postId, rating, review)
    }


}