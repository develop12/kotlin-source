package com.project.sherby.sherby.data.sendMessage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class SendMessagePojo : Serializable {

    @SerializedName("success")
    @Expose
    var success: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

}