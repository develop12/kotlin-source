package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.singlelisting.Rating
import kotlinx.android.synthetic.main.item_ratings.view.*

class AdapterRatings(private val items: ArrayList<Rating>, private val context: Context) : RecyclerView.Adapter<AdapterRatings.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return AdapterRatings.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_ratings, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvReview.text = items[position].review
        holder.ratingsBar.rating = items[position].rating?.toFloat()!!
        holder.name.text=items[position].userName
        Glide.with(context).applyDefaultRequestOptions(RequestOptions().placeholder(R.mipmap.placeholder)).load(items[position].userImage).into(holder.pic)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ratingsBar = view.ratingBar3!!
        val tvReview = view.tvReviewSingleList!!

        val name = view.tvReviewerName!!
        val pic = view.ivReviewerPic!!

    }
}