package com.project.sherby.sherby.other

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.icu.util.Calendar
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.AlertDialogListener
import com.project.sherby.sherby.utils.Constants
import java.io.ByteArrayOutputStream


/**
 * Created by android on 2/11/17.
 */
open class BaseFragment : Fragment() {
    private val PERMISSION_REQUEST = 121

    protected var DATE_FORMAT = "yyyy-MM-dd"
    protected val SEVEN_DAYS = 7 * 60 * 60 * 1000
    protected val overview = 0
    protected val TAG = javaClass.simpleName
    protected var mContent: View? = null// For showing snackbar
    private var mActivity: FragmentActivity? = null

    private var mProgressDialog: Dialog? = null
    private lateinit var mCalendar: java.util.Calendar
    private var mStartTime: Calendar? = null
    private var mEndTime: Calendar? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        mActivity = activity

    }

    fun showLoading(show: Boolean?) {
        if (show!!) showProgress() else hideProgress()
    }

    private fun showProgress() {
        if (mProgressDialog == null) {

            mProgressDialog = Dialog(mActivity, android.R.style.Theme_Translucent)
            mProgressDialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            mProgressDialog?.setContentView(R.layout.loader_half__layout)
            mProgressDialog?.setCancelable(false)

        }

        mProgressDialog?.show()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContent = view
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mStartTime = Calendar.getInstance()
            mEndTime = Calendar.getInstance()
            mCalendar = java.util.Calendar.getInstance()
        }

    }

    fun View.visible() {
        this.visibility = View.INVISIBLE
    }

    fun showSnackBar(message: String) {
        mContent?.let {

            val snackbar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            val snackbarView = snackbar.view
            val tv = snackbarView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
            tv.maxLines = 3
            snackbar.duration = Constants.SNACK_BAR_DURATION
            snackbar.show()

        }
    }


    override fun onPause() {
        super.onPause()

    }

    override fun onStart() {
        super.onStart()
    }

    fun canGetLocation(): Boolean {
        return isLocationEnabled(requireContext()) // application context
    }

    fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        var locationProviders: String

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
            }
            locationMode != Settings.Secure.LOCATION_MODE_OFF
        } else {
            locationProviders = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_MODE)
            !TextUtils.isEmpty(locationProviders)
        }
    }

    /**
     * Add fragment with or without addToBackStack
     *
     * @param fragment       which needs to be attached
     * @param addToBackStack is fragment needed to backstack
     */
    fun addFragment(fragment: Fragment, addToBackStack: Boolean, id: Int) {
        val tag = fragment.javaClass.simpleName
        val fragmentManager = activity?.supportFragmentManager
        val fragmentOldObject = fragmentManager?.findFragmentByTag(tag)
        val transaction = fragmentManager?.beginTransaction()
        transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out, R.anim.anim_in_reverse, R.anim.anim_out_reverse)
        if (fragmentOldObject != null) {
            fragmentManager.popBackStackImmediate(tag, 0)
        } else {
            if (addToBackStack) {
                transaction?.addToBackStack(tag)
            }
            transaction?.add(id, fragment, tag)
                    ?.commitAllowingStateLoss()
        }
    }

    //for future use
    fun addFragmentForFlipTransition(fragment: Fragment, addToBackStack: Boolean) {
        val tag = fragment.javaClass.simpleName
        val fragmentManager = mActivity?.supportFragmentManager
        val fragmentOldObject = fragmentManager?.findFragmentByTag(tag)
        val transaction = fragmentManager?.beginTransaction()
        transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out, R.animator.right_in, R.animator.right_out)
        if (fragmentOldObject != null) {
            fragmentManager.popBackStackImmediate(tag, 0)
        } else {
            if (addToBackStack) {
                transaction?.addToBackStack(tag)
            }
            transaction?.replace(R.id.rvFavourites, fragment, tag)
                    ?.commitAllowingStateLoss()
        }
    }


    protected fun setProfileImage(imagePath: String?, imageView_profile: ImageView, progressBar: ProgressBar?) {
//        GlideApp.with(mmActivity)
//                .load(imagePath)
//                .placeholder(R.drawable.ic_add_media)
//                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                .centerCrop()
//                .into(imageView_profile)
    }

    protected fun goBack() {
        mActivity?.onBackPressed()
    }

//    fun showProgress() {
//        if (mProgressDialog == null) {
//            mProgressDialog = Dialog(mmActivity, android.R.style.Theme_Translucent)
//            mProgressDialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
//            mProgressDialog?.setContentView(R.layout.loader_half__layout)
//            mProgressDialog?.setCancelable(false)
//
//        }
//
//        mProgressDialog?.show()
//    }

    fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog?.dismiss()
        }
    }

//    fun showMessage(message: String) {
//        Utils.showSnackbar(mContent, message)
//    }


//    fun showLoading(show: Boolean?) {
//        if (show!!) showProgress() else hideProgress()
//    }


    fun checkForPermission(): Boolean {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission()
                return false
            }
        }
        return true
    }


    /**
     * This method will request permission
     */
    private fun requestPermission() {

        ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                Constants.PERMISSION_REQUEST_CODE)

    }


    /**
     * This will show permission dialog
     */

    fun permissionDenied() {
        val builder = android.support.v7.app.AlertDialog.Builder(requireContext())

        builder.setMessage(getString(R.string.permission_denied))

        val positiveText = getString(android.R.string.ok)
        builder.setPositiveButton(positiveText
        ) { dialog, which ->
            enablePermission()
        }

        val negativeText = getString(android.R.string.cancel)
        builder.setNegativeButton(negativeText
        ) { dialog, which ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        // display dialog
        dialog.show()
    }


    private fun enablePermission() {
        val packageName = mActivity!!.packageName;

        try {
            //Open the specific App Info page:
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.data = Uri.parse("package:$packageName")
            startActivityForResult(intent, PERMISSION_REQUEST)

        } catch (e: ActivityNotFoundException) {
            //e.printStackTrace();
            //Open the generic Apps page:
            val intent = Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
            startActivityForResult(intent, PERMISSION_REQUEST)

        }

    }

    fun replaceFragment(fragment: Fragment, animate: Boolean, id: Int) {
        val tag: String = fragment::class.java.simpleName

        val transaction = activity!!.supportFragmentManager?.beginTransaction()
        if (animate) {
            transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out,
                    R.anim.anim_in_reverse, R.anim.anim_out_reverse)
        }
        transaction?.replace(id, fragment, tag)
                ?.commitAllowingStateLoss()
    }

    fun replaceChatFragment(fragment: Fragment, animate: Boolean) {
        val tag: String = fragment::class.java.simpleName

        val transaction = activity!!.supportFragmentManager?.beginTransaction()
        if (animate) {
            transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out,
                    R.anim.anim_in_reverse, R.anim.anim_out_reverse)

        }
        transaction?.replace(R.id.frame_nested_chats, fragment, tag)
                ?.commitAllowingStateLoss()
    }

    fun checkForStoragePermission(): Boolean {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                return false
            }
        }
        return true
    }


    fun showAlertDialog(pos: Int, heading: String, message: String, listener: AlertDialogListener?) {
        val builder = AlertDialog.Builder(context).apply {
            setTitle(heading)
            setMessage(message)
        }

        val positiveText = getString(android.R.string.ok)
        builder.setPositiveButton(positiveText
        ) { dialog, which ->
            // positive button logic
            dialog.dismiss()
            listener?.actionOk(pos)

        }

        val negativeText = getString(android.R.string.cancel)
        builder.setNegativeButton(negativeText
        ) { dialog, which ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        // display dialog
        dialog.show()
    }


    fun goBackWithDelay() {
        Handler().postDelayed({ goBack() }, Constants.HANDLER_DELAY_TIME)
    }


    /***************************** Extension functions for image loading **********************************/


    fun ImageView.loadImage(url: String) {

        val requestOptions = RequestOptions.placeholderOf(R.mipmap.placeholder)
        Glide.with(this)
                .applyDefaultRequestOptions(requestOptions)
                .load(url)
                .into(this)

    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path: String = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title",
                null)
        return Uri.parse(path)
    }

}