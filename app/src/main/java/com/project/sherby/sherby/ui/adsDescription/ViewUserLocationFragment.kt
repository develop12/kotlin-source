package com.project.sherby.sherby.ui.adsDescription

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.utils.Constants


/**
 * Created by ${Shubham} on 6/6/2018.
 */
class ViewUserLocationFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    private var mMarcadorActual: Marker? = null
    private var latLng: LatLng? = null

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
//        val sydney = latLng
        mMarcadorActual = mMap!!.addMarker(MarkerOptions().position(latLng!!))
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f))
    }

    override fun onClick(v: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_view_user_location, container, false)


        val mapFragment = childFragmentManager.findFragmentById(R.id.map_location) as SupportMapFragment
        mapFragment.getMapAsync(this)


        return rootView

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //val mapFragment = activity!!.fragmentManager.findFragmentById(R.id.map_location) as MapFragment
        getBundledArguments()
    }

    private fun getBundledArguments() {
        val bundle: Bundle = this.arguments!!

        if (!bundle.isEmpty) {

            latLng = LatLng(bundle.getString(Constants.LATITUDE).toDouble(),
                    bundle.getString(Constants.LONGITUDE).toDouble())

        }
    }

}