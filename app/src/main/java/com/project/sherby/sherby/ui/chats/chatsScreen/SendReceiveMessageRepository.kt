package com.project.sherby.sherby.ui.chats.chatsScreen

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.receivedBidMessages.BidPojo
import com.project.sherby.sherby.data.sendMessage.SendMessagePojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object SendReceiveMessageRepository {

    private val webService = ApiHelper.createService()


    fun sendMessage(successHandler: (SendMessagePojo) -> Unit,
                    failureHandler: (String) -> Unit, postId: String,
                    messageContent: String,
                    fromUserId: String) {
        webService.sendMessage(postId, messageContent, fromUserId).enqueue(object : Callback<SendMessagePojo> {
            override fun onResponse(call: Call<SendMessagePojo>?, response: Response<SendMessagePojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)

                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<SendMessagePojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }


    fun receiveMessages(successHandler: (BidPojo) -> Unit,
                        failureHandler: (String) -> Unit,
                        postId: String, userId: String) {
        webService.receiveMessages(postId, userId).enqueue(object : Callback<BidPojo> {
            override fun onResponse(call: Call<BidPojo>?, response: Response<BidPojo>?) {
                response?.body()?.let { it ->
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<BidPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }
}