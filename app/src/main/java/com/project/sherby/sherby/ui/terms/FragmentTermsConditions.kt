package com.project.sherby.sherby.ui.terms

import android.os.Bundle
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.BaseActivity
import com.project.sherby.sherby.utils.Constants
import kotlinx.android.synthetic.main.fragment_terms_and_conditions.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream

/**
 * Created by ${Shubham} on 8/2/2018.
 */
class FragmentTermsConditions : BaseActivity() {
    private var value: Int = 0
    override fun getID(): Int {
        return R.layout.fragment_terms_and_conditions
    }

    override fun iniView(savedInstanceState: Bundle?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_terms_and_conditions)
        setData()
    }

    private fun setData() {
        value = intent.getIntExtra(Constants.VALUE_FOR_TEXT_FILE, 0)
        tvTextFileReaderRaw.text = readTxt()
    }

    private fun readTxt(): String {
        var inputStream: InputStream? = null
        when (value) {
            1 -> {
                tvHeader.text = "HELP"
                inputStream = resources.openRawResource(R.raw.about_us)
            }

            2 -> {
                tvHeader.text = "TERMS AND CONDITIONS"
                inputStream = resources.openRawResource(R.raw.terms_conditions)
            }

            3 -> {
                tvHeader.text = "PRIVACY POLICY"
                inputStream = resources.openRawResource(R.raw.privacy_policy)
            }
        }

        val byteArrayOutputStream = ByteArrayOutputStream()
        var i: Int
        try {
            i = inputStream!!.read()
            while (i != -1) {
                byteArrayOutputStream.write(i)
                i = inputStream.read()
            }
            inputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return byteArrayOutputStream.toString()
    }
}