package com.project.sherby.sherby.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.project.sherby.sherby.R
import com.project.sherby.sherby.ui.login.defaultView.DefaultViewFragment
import com.project.sherby.sherby.ui.login.login.LoginFragment
import com.project.sherby.sherby.ui.login.signup.SignUpFragment
import com.project.sherby.sherby.utils.BaseActivity
import kotlinx.android.synthetic.main.included_top_signup.*


/**
 * Created by ${Shubham} on 6/4/2018.
 */
class LoginActivity : BaseActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v) {
            tvLogin -> {
                replaceFragment(LoginFragment(), R.id.frame_login)
            }

            tvSignUpButton -> {
                replaceFragment(SignUpFragment(), R.id.frame_login)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loadInitialFragment()
        clickListeners()
    }


    override fun getID(): Int {
        return R.layout.activity_login
    }

    override fun iniView(savedInstanceState: Bundle?) {
    }

    private fun clickListeners() {
        tvLogin.setOnClickListener(this)
        tvSignUpButton.setOnClickListener(this)
    }

    private fun loadInitialFragment() {

        addFragment(DefaultViewFragment(), false, R.id.frame_login)
    }

    override fun onBackPressed() {
        startActivity(Intent(this@LoginActivity, com.project.sherby.sherby.activity.LauncherActivity::class.java))
        finish()
    }
}