package com.project.sherby.sherby.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterReportUser.ViewHolder
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_report_recyclerview.view.*

/**
 * Created by ${Shubham} on 7/18/2018.
 */
class AdapterReportUser(private val items: ArrayList<String>, private val context: Context,
                        private val clickListener: RecyclerViewClickListener) : RecyclerView.Adapter<ViewHolder>() {

    private var selectedPosition: Int = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_report_recyclerview, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvReason.text = items[position]
        holder.init(clickListener)

        if (position == selectedPosition)
            holder.tvReason.setTextColor(context.resources.getColor(R.color.themeColor))
        else
            holder.tvReason.setTextColor(Color.BLACK)

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvReason: TextView = view.tvReason

        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {

                click.onClick(itemView, adapterPosition, tvReason.text.toString())
                selectedPosition = adapterPosition
                notifyDataSetChanged()

            }
        }
    }
}