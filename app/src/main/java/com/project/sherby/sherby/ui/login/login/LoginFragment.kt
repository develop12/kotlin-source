package com.project.sherby.sherby.ui.login.login

import Preferences
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.iid.FirebaseInstanceId
import com.project.sherby.sherby.R
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.login.forgotPassword.ForgotPasswordFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.Validations
import com.project.sherby.sherby.utils.saveValue
import kotlinx.android.synthetic.main.included_top_signup.*
import kotlinx.android.synthetic.main.nested_fragment_signin.*

/**
 * Created by ${Shubham} on 6/4/2018.
 */
class LoginFragment : BaseFragment(), View.OnClickListener {
    private var mViewModel: LoginModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(LoginModel::class.java)
        attachObservers()
    }

    override fun onClick(v: View?) {

        when (v) {
            tvLoginButton -> {
                if (Validations.isEmpty(etUserName)) {
                    if (Validations.isEmpty(etPasswordSignUp)) {
                        if (Validations.isValidPassword(etPasswordSignUp)) {
                            Preferences.prefs!!.saveValue(Constants.USER_EMAIL, etUserName.text.toString())
                            mViewModel!!.authenticate(etUserName.text.toString().trim(),
                                    etPasswordSignUp.text.toString().trim(),
                                    FirebaseInstanceId.getInstance().token!!,
                                    "android"
                            )
                        }
                    }
                }
            }

            tvForgotYourPass -> {
                //addFragment(ForgotPasswordFragment(),true,R.id.frame_login)
                startActivity(Intent(activity, ForgotPasswordFragment::class.java))
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.nested_fragment_signin, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        clickListeners()
    }

    private fun initViews() {

        activity!!.tvLogin.setBackgroundResource(R.drawable.login_top_right_filled_background)
        activity!!.tvLogin.setTextColor(resources.getColor(android.R.color.white))
        activity!!.tvSignUpButton.setBackgroundResource(R.drawable.login_top_left_background)
        activity!!.tvSignUpButton.setTextColor(resources.getColor(R.color.themeColor))

    }


    private fun clickListeners() {
        tvLoginButton.setOnClickListener(this)
        tvForgotYourPass.setOnClickListener(this)
    }


    private fun attachObservers() {
        mViewModel?.response?.observe(this, Observer {
            it?.let {
                val message = "${it.message}"
                showSnackBar(message)
                if (it.success == 1) {
                    Preferences.prefs?.saveValue(Constants.IS_LOGGED_IN, true)
                    Preferences.prefs?.saveValue(Constants.USER_ID, it.userId)
                    goBack()
                }
            }
        })
        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })
    }

}