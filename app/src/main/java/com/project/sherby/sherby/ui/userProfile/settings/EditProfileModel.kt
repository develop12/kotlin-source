package com.project.sherby.sherby.ui.userProfile.settings

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.MyViewModel
import okhttp3.RequestBody

/**
 * Created by android on 5/4/18.
 */
class EditProfileModel : MyViewModel() {

    var response = MutableLiveData<RegisterUserPojo>()
    var responseChangePAssword = MutableLiveData<RegisterUserPojo>()

    fun editProfile(params: Map<String, RequestBody>) {

        isLoading.value = true
        EditProfileRepository.editProfileData({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, params)
    }


    fun changePassword(userId: String,
                       oldPassword: String,
                       newPassword: String) {

        isLoading.value = true
        EditProfileRepository.changePassword({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, userId, oldPassword, newPassword)
    }
}