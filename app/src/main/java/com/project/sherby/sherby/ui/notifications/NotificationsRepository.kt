package com.project.sherby.sherby.ui.notifications

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.notifications.NotificationPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by ${Shubham} on 7/24/2018.
 */
object NotificationsRepository {


    private val webService = ApiHelper.createService()

    fun fetchNotifications(successHandler: (NotificationPojo) ->
    Unit, failureHandler: (String) -> Unit, userId: String) {
        webService.notificationTab(userId).enqueue(object : Callback<NotificationPojo> {
            override fun onResponse(call: Call<NotificationPojo>?, response: Response<NotificationPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }

            override fun onFailure(call: Call<NotificationPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }

        })
    }
}