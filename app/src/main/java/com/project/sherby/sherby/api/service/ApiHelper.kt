package com.project.sherby.sherby.api.service

import com.google.gson.GsonBuilder
import com.project.sherby.sherby.data.ErrorResponse
import com.project.sherby.sherby.data.Status
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.security.ResponseInterceptorterceptor
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


object ApiHelper {


    private var mRetrofit: Retrofit

    var gson = GsonBuilder()
            .setLenient()
            .create()

    // Creating Retrofit Object
    init {
        mRetrofit = Retrofit.Builder()
                .baseUrl(Constants.FINAL_BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getClient())
                .build()
    }

    // Creating OkHttpclient Object
    private fun getClient(): OkHttpClient {

        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient().newBuilder().connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .addInterceptor(ChuckInterceptor(com.project.sherby.sherby.application.Application.getContext()))
                .addInterceptor(ResponseInterceptorterceptor())
                .build()

    }

    //Creating service class for calling the web services
    fun createService(): WebService {
        return mRetrofit.create(WebService::class.java)
    }

    // Handling error messages returned by Apis
    fun handleApiError(body: ResponseBody?): String {
        var errorMsg = Constants.SOMETHING_WENT_WRONG
        try {
            val errorConverter: Converter<ResponseBody, Status> =
                    mRetrofit.responseBodyConverter(Status::class.java, arrayOfNulls(0))
            val error: Status = errorConverter.convert(body)
            errorMsg = error.message
        } catch (e: Exception) {
        }
        return errorMsg
    }

    fun handleAuthenticationError(body: ResponseBody?): String {
        val errorConverter: Converter<ResponseBody, ErrorResponse> =
                mRetrofit.responseBodyConverter(ErrorResponse::class.java, arrayOfNulls(0))
        val errorResponse: ErrorResponse = errorConverter.convert(body)
        var errorMsg = errorResponse.message!!
        val email = errorResponse.errors?.email
        val password = errorResponse.errors?.password
        val role = errorResponse.errors?.role
        val deviceToken = errorResponse.errors?.deviceToken
        val mobileNumber = errorResponse.errors?.mobile

        if (email != null && email.isNotEmpty()) {
            errorMsg = email[0].toString()
        } else if (password != null && password.isNotEmpty()) {
            errorMsg = password[0].toString()
        } else if (role != null && role.isNotEmpty()) {
            errorMsg = role[0].toString()
        } else if (deviceToken != null && deviceToken.isNotEmpty()) {
            errorMsg = (deviceToken[0].toString())
        } else if (mobileNumber != null && mobileNumber.isNotEmpty()) {
            errorMsg = mobileNumber[0].toString()
        }
        return errorMsg
    }

}