package com.project.sherby.sherby.ui.chats.chatsScreen

import Preferences
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterChatScreen
import com.project.sherby.sherby.utils.BaseActivity
import com.project.sherby.sherby.utils.Constants
import kotlinx.android.synthetic.main.chat_recycler_view.*
import java.util.*


/**
 * Created by ${Shubham} on 6/21/2018.
 */
class ActivityChatScreen : BaseActivity(), View.OnClickListener {

    private var mViewModel: SendMessageModel? = null
    private var adapterChatScreen: AdapterChatScreen? = null
    private var value: String = ""
    private var postID: String = ""
    private var chatList: ArrayList<com.project.sherby.sherby.data.receivedBidMessages.Chat>? = ArrayList()

    override fun iniView(savedInstanceState: Bundle?) {

    }

    override fun getID(): Int {
        return R.layout.chat_recycler_view
    }


    override fun onClick(v: View?) {
        when (v) {
            btSendMessage -> {

                mViewModel?.sendMessage(postID, etMessage.text.toString(),
                        Preferences.prefs?.getString(Constants.USER_ID, "0")!!)


                setUpRecyclerView()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chatList!!.clear()

        mViewModel = ViewModelProviders.of(this).get(SendMessageModel::class.java)

        attachObservers()

        setContentView(R.layout.chat_recycler_view)
        getBundledData()
        clickListeners()
        setData()


    }

    private fun clickListeners() {
        btSendMessage.setOnClickListener(this)
    }

    private fun getBundledData() {

        val bundle = intent.extras
//        value = bundle!!.getString(Constants.USER_NAME)
        tvToolBar.text = bundle!!.getString(Constants.USER_NAME)
        postID = bundle.getString(Constants.POST_ID)

        mViewModel?.receiveMessages(postID, Preferences.prefs?.getString(Constants.USER_ID, "0")!!)

        adapterChatScreen = AdapterChatScreen(this, this.chatList!!)

    }

    private fun setData() {
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        linearLayoutManager.reverseLayout = true
        rvSingleUserChat.layoutManager = linearLayoutManager
        rvSingleUserChat.adapter = adapterChatScreen


    }


    private fun attachObservers() {
        mViewModel?.response?.observe(this, Observer {
            it?.let {
                //                chatList.add(it.chat)
                if (it.success == 1) {
                    mViewModel?.receiveMessages(postID, Preferences.prefs?.getString(Constants.USER_ID, "0")!!)
                }
            }
        })
        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                //                            showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })




        mViewModel?.responseReceiveMessages?.observe(this, Observer {
            it?.let {
                if (it.success == 1) {
                    tvChatUserName.text = it.response!!.title
                    tvBidAmount.text = it.response!!.bidamount

                    val requestOptions = RequestOptions()
                    requestOptions.placeholder(R.drawable.ic_person)
                    Glide.with(this).load(it.response!!.featuredImage)
                            .apply(RequestOptions.bitmapTransform(CircleCrop()))
                            .apply(requestOptions)
                            .into(ivChatUser)

                    chatList = it.response!!.chat as ArrayList<com.project.sherby.sherby.data.receivedBidMessages.Chat>?
                    setUpRecyclerView()
                }
            }
        })
        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                //                            showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })
    }

    private fun setUpRecyclerView() {

        chatList!!.reverse()

        adapterChatScreen = AdapterChatScreen(this, this.chatList!!)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        linearLayoutManager.reverseLayout = true
        rvSingleUserChat.layoutManager = linearLayoutManager
        rvSingleUserChat.adapter = adapterChatScreen

        adapterChatScreen!!.notifyDataSetChanged()
        etMessage.setText("")

    }
}