package com.project.sherby.sherby.ui.report

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


object ReportPostRepository {

    private val webService = ApiHelper.createService()

    fun fetchHomeData(successHandler: (RegisterUserPojo) ->
    Unit, failureHandler: (String) -> Unit, userId: String,
                      postId: String,
                      reason: String,
                      msg: String) {
        webService.report(userId, postId, reason, msg).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }

        })
    }


}
