package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.home.Category
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_drawer_recyclerview.view.*


class AdapterDrawerItems(private val items: ArrayList<Category>, private val context: Context, private val recyclerViewClickListener: RecyclerViewClickListener)
    : RecyclerView.Adapter<AdapterDrawerItems.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName.text = items[position].name
        holder.init(recyclerViewClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_drawer_recyclerview, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvName = view.tvDrawerItem!!
        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {
                click.onClick(itemView, adapterPosition, tvName.text.toString())
            }
        }
    }
}