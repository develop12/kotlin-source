package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.home.Listing
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_home_recycler.view.*


class HomeItemAdapter(private val items: ArrayList<Listing>, private val context: Context,
                      private val clickListener: RecyclerViewClickListener)
    : RecyclerView.Adapter<HomeItemAdapter.ViewHolder>(), Filterable {

    private var mFilteredList: ArrayList<Listing>? = items


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {

                val charString = charSequence.toString()

                if (charString.isEmpty()) {

                    mFilteredList = items
                } else {

                    val filteredList = ArrayList<Listing>()

                    for (androidVersion in items) {

                        if (androidVersion.name!!.toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion)
                        }
                    }

                    mFilteredList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = mFilteredList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                mFilteredList = filterResults.values as ArrayList<Listing>
                notifyDataSetChanged()
            }
        }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
        Glide.with(context).load(mFilteredList!![position].image).apply(requestOptions).into(holder.ivImage)
        holder.tvName.text = mFilteredList!![position].name
        holder.tvPrice.text = mFilteredList!![position].price
        holder.tvLocationAddress.text = mFilteredList!![position].location
        holder.init(clickListener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_home_recycler, parent, false))
    }

    override fun getItemCount(): Int {
        return mFilteredList!!.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivImage = view.ivProductImage!!
        val tvName = view.tvProductName!!
        val tvPrice = view.tvPrice!!
        val tvLocationAddress = view.tvLocationAddress!!

        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {
                click.onClick(itemView, adapterPosition, mFilteredList!![adapterPosition].id.toString())
            }
        }
    }

}