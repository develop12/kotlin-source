package com.project.sherby.sherby.data.singlelisting

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class SingleListingPojo : Serializable {

    @SerializedName("success")
    @Expose
    var success: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("Post_data")
    @Expose
    var post_data: Post_data? = null

}