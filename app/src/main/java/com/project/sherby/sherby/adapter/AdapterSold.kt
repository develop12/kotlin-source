package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.userProfile.Sold
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_home_recycler.view.*

class AdapterSold(private val items: ArrayList<Sold>, private val context: Context,
                  private val clickListener: RecyclerViewClickListener) : RecyclerView.Adapter<AdapterSold.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return AdapterSold.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_home_recycler, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))


        Glide.with(context).load(items[position].image).apply(requestOptions).into(holder.ivImage)
        holder.tvName.text = items[position].name


//        holder.tvPrice.text = items[position].price
//        holder.tvLocationAddress.text = items[position].location
        holder.init(clickListener)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivImage = view.ivProductImage!!
        val tvName = view.tvProductName!!
        val tvPrice = view.tvPrice!!
        val tvLocationAddress = view.tvLocationAddress!!

        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {
                click.onClick(itemView, adapterPosition, "f")
            }
        }
    }
}