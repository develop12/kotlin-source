package com.project.sherby.sherby.activity

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.countries.CountriesPojo
import com.project.sherby.sherby.data.home.Category
import com.project.sherby.sherby.data.pickPackage.PickPackagePojo
import com.project.sherby.sherby.utils.MyViewModel

class CategoryFetchModel : MyViewModel() {

    var response = MutableLiveData<List<Category>>()
    var responsePakages = MutableLiveData<PickPackagePojo>()
    var responseCountries = MutableLiveData<CountriesPojo>()


    fun fetchCategories() {

        //isLoading.value = true
        FetchCategoryRepository.fetchCategories({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        })
    }

    fun fetchPackages() {

        isLoading.value = true
        FetchCategoryRepository.fetchPackages({
            responsePakages.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        })
    }

    fun fetchCountries() {

        isLoading.value = true
        FetchCategoryRepository.fetchCountries({
            responseCountries.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        })
    }

}