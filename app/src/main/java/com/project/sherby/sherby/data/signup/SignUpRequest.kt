package com.project.sherby.sherby.data.signup

import com.google.gson.annotations.SerializedName

/**
 * Created by ${Shubham} on 6/5/2018.
 */
data class SignUpRequest(@field:SerializedName("user_login") var user_login: String,
                         @field:SerializedName("user_email") var user_email: String,
                         @field:SerializedName("user_pass") var user_pass: String,
                         @field:SerializedName("user_phone") var user_phone: String)