package com.project.sherby.sherby.adapter

import Preferences
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.Constants
import kotlinx.android.synthetic.main.item_sent_messages.view.*

/**
 * Created by ${Shubham} on 6/21/2018.
 */
class AdapterChatScreen(var context: Context, var chatList: ArrayList<com.project.sherby.sherby.data.receivedBidMessages.Chat>) : RecyclerView.Adapter<AdapterChatScreen.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvSentMessage.text = chatList[position].messageContent
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvSentMessage = view.tvSentMessages!!
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == 0)
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_sent_messages, parent, false))
        else
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_received_messages, parent, false))
    }

    override fun getItemCount(): Int {
        return chatList.size
    }


    override fun getItemViewType(position: Int): Int {
        return if (chatList[position].senderId == Preferences.prefs?.getString(Constants.USER_ID, "0")!!)
            0
        else
            1
    }
}