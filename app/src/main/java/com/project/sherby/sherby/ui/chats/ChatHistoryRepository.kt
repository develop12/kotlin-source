package com.project.sherby.sherby.ui.chats

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.chatHistory.ChatHistoryPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by android on 29/3/18.
 */
object ChatHistoryRepository {

    private val webService = ApiHelper.createService()

    fun getData(successHandler: (ChatHistoryPojo) -> Unit, failureHandler: (String) -> Unit, doctorId: String) {
        webService.chatHistory(doctorId).enqueue(object : Callback<ChatHistoryPojo> {
            override fun onResponse(call: Call<ChatHistoryPojo>?, response: Response<ChatHistoryPojo>?) {
                response?.body()?.let {
                    successHandler(it)

                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<ChatHistoryPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }

}