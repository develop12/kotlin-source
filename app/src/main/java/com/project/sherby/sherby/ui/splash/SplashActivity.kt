package com.project.sherby.sherby.ui.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.crashlytics.android.Crashlytics
import com.project.sherby.sherby.R
import com.project.sherby.sherby.activity.LauncherActivity
import com.project.sherby.sherby.utils.BaseActivity
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_splash.*


/**
 * Created by ${Shubham} on 7/4/2018.
 */
class SplashActivity : BaseActivity() {
    private var intents: Intent? = null
    override fun getID(): Int {
        return R.layout.activity_splash
    }

    override fun iniView(savedInstanceState: Bundle?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Fabric.with(this, Crashlytics())
        setData()
    }

    private fun setData() {
        intents = Intent(this, LauncherActivity::class.java)
        if (intent.hasExtra("fromNotif")) {
            intents!!.putExtra("fromNotif","1")
        }
        val video = Uri.parse("android.resource://" + packageName + "/" + R.raw.sherbby)
        splashVideo.setVideoURI(video)
        splashVideo.setOnCompletionListener { startNextActivity() }
        splashVideo.start()


    }

    private fun startNextActivity() {
        startActivity(intents)
        finish()
    }
}