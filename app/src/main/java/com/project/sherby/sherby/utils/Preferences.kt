import android.content.Context
import com.project.sherby.sherby.utils.EncryptedPreferences

/**
 * Created by android on 27/2/18.
 */
public class Preferences {
    companion object {

        var prefs: EncryptedPreferences? = null
        fun initPreferences(context: Context) {
            prefs = EncryptedPreferences(context)
        }
    }
}