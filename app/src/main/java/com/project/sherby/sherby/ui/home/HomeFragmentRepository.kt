package com.project.sherby.sherby.ui.home

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.favourites.FetchFavouritesPojo
import com.project.sherby.sherby.data.home.HomeItemsPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


object HomeFragmentRepository {

    private val webService = ApiHelper.createService()

    fun fetchHomeData(successHandler: (HomeItemsPojo) ->
    Unit, failureHandler: (String) -> Unit,/*address: String,*/ catid: String, currentPage: String) {
        webService.listing(/*address,*/catid, currentPage).enqueue(object : Callback<HomeItemsPojo> {
            override fun onResponse(call: Call<HomeItemsPojo>?, response: Response<HomeItemsPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }

            override fun onFailure(call: Call<HomeItemsPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }

        })
    }


    fun fetchFavourites(successHandler: (FetchFavouritesPojo) ->
    Unit, failureHandler: (String) -> Unit, userId: String) {
        webService.fetchFavourites(userId).enqueue(object : Callback<FetchFavouritesPojo> {
            override fun onResponse(call: Call<FetchFavouritesPojo>?, response: Response<FetchFavouritesPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }


            override fun onFailure(call: Call<FetchFavouritesPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }
        })
    }


    fun fetchLocationData(successHandler: (HomeItemsPojo) ->
    Unit, failureHandler: (String) -> Unit, address: String, catid: String, currentPage: String) {
        webService.filter(address, catid, currentPage).enqueue(object : Callback<HomeItemsPojo> {
            override fun onResponse(call: Call<HomeItemsPojo>?, response: Response<HomeItemsPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }

            override fun onFailure(call: Call<HomeItemsPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(it.localizedMessage)
                }
            }

        })
    }

}
