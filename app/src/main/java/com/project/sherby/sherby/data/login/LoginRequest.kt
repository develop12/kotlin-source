package com.project.sherby.sherby.data.login

data class LoginRequest(var user_login: String, var user_pass: String)