package com.project.sherby.sherby.ui.report

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.MyViewModel

class ReportPostModel : MyViewModel() {

    var response = MutableLiveData<RegisterUserPojo>()


    fun fetchData(userId: String,
                  postId: String,
                  reason: String,
                  msg: String) {

        isLoading.value = true
        ReportPostRepository.fetchHomeData({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, userId, postId, reason, msg)
    }


}