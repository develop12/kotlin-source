package com.project.sherby.sherby.ui.userProfile

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import com.project.sherby.sherby.data.userProfile.Favourite
import com.project.sherby.sherby.data.userProfile.GetUserProfilePojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by android on 29/3/18.
 */
class UserProfileModel : MyViewModel() {

    var response = MutableLiveData<GetUserProfilePojo>()

    fun userDetails(doctorId: String) {

        isLoading.value = true
        UserDetailRepository.getUserProfile({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, doctorId)
    }



    //**delete**//




}