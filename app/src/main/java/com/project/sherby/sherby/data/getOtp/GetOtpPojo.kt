package com.project.sherby.sherby.data.getOtp

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetOtpPojo : Serializable {

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("verification_code")
    @Expose
    var verification_code: String? = null


}