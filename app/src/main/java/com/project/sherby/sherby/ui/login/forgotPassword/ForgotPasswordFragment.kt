package com.project.sherby.sherby.ui.login.forgotPassword

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.BaseActivity
import kotlinx.android.synthetic.main.fragment_forgot_password.*

/**
 * Created by ${Shubham} on 7/25/2018.
 */
class ForgotPasswordFragment : BaseActivity(), View.OnClickListener {
    override fun getID(): Int {
        return R.layout.fragment_forgot_password
    }

    override fun iniView(savedInstanceState: Bundle?) {

    }

    private var mViewModel: ForgotPasswordViewModel? = null

    override fun onClick(v: View?) {
        when (v) {
            tvSubmitForgot -> {
                mViewModel?.authenticate(etNumberForgot.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachObservers()
        setContentView(R.layout.fragment_forgot_password)
        mViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)
        clickListeners()
    }

    private fun attachObservers() {

        mViewModel?.response?.observe(this, Observer {
            it?.let {
                //                 val message = "${it.message}"
//                 showSnackBar(message,)
                if (it.success == 1) {
                    Toast.makeText(this, "Password Sent", Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }

            }
        })
        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                // showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })

    }

    private fun clickListeners() {
        tvSubmitForgot.setOnClickListener(this)
    }
}