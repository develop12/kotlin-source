package com.project.sherby.sherby.data.chatHistory

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class ChatHistoryPojo : Serializable {

    @SerializedName("success")
    @Expose
    var success: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("chat_list")
    @Expose
    var chat_list: List<Chat_list>? = null

}