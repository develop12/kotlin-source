package com.project.sherby.sherby.ui.makeAnOffer

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by ${Shubham} on 7/23/2018.
 */
object MakeAnOfferRepository {


    private val webService = ApiHelper.createService()

    fun makeAnOffer(successHandler: (RegisterUserPojo) -> Unit, failureHandler: (String) -> Unit, postId: String,
                    bidAmount: String,
                    fromUserId: String,
                    messageContent: String) {
        MakeAnOfferRepository.webService.makeOffer(postId, bidAmount, fromUserId, messageContent).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)

                    }
                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }
}