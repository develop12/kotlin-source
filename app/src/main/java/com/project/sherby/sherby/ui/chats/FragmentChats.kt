package com.project.sherby.sherby.ui.chats

import Preferences
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterChats
import com.project.sherby.sherby.data.chatHistory.Chat_list
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.chats.chatsScreen.ActivityChatScreen
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.fragment_chats.*

/**
 * Created by ${Shubham} on 5/30/2018.
 */
class FragmentChats : BaseFragment(), View.OnClickListener, RecyclerViewClickListener {
    override fun onClick(view: View, position: Int, url: String) {
        val bundle = Bundle()
        bundle.putString(Constants.USER_NAME, items[position].user_name)
        bundle.putString(Constants.POST_ID, items[position].list_id)

        val intent = Intent(activity, ActivityChatScreen::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private var mViewModel: ChatHistoryModel? = null
    private var items: ArrayList<Chat_list> = ArrayList()
    override fun onClick(v: View?) {
        when (v) {
            rvChats -> {

            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(ChatHistoryModel::class.java)
        attachObservers()
    }

    private fun attachObservers() {
        mViewModel?.response?.observe(this, Observer {
            it?.let {
                if (it.success == 1) {
                    items = it.chat_list as ArrayList<Chat_list>
                    setUpRecyclerView()
                }
            }
        })
        mViewModel?.apiError?.observe(this, Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, Observer {
            it?.let { showLoading(it) }
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chats, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel?.chatHistory(Preferences.prefs?.getString(Constants.USER_ID, "0")!!)


        clickListener()
    }

    private fun clickListener() {
        rvChats.setOnClickListener(this)
    }

    private fun setUpRecyclerView() {

        rvChats.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,
                false)
        rvChats.adapter = AdapterChats(items, requireContext(), this)
//        rvChats.visible()
    }


}