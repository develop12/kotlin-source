package com.project.sherby.sherby.ui.userProfile.mylistings

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.favourites.FetchFavouritesPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by ${Shubham} on 7/5/2018.
 */
class MyListingsModel : MyViewModel() {
    var response = MutableLiveData<FetchFavouritesPojo>()

    fun myListings(userId: String) {
        isLoading.value = true
        MyListingsRepository.getMyListings({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, userId)
    }
}