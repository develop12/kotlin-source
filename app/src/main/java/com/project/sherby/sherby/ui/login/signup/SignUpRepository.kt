package com.project.sherby.sherby.ui.login.signup

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.getOtp.GetOtpPojo
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by android on 19/2/18.
 *
 * Data Provider for the application
 */
object SignUpRepository {
    private val webService = ApiHelper.createService()

    fun registerUser(successHandler: (RegisterUserPojo)
    -> Unit, failureHandler: (String) -> Unit,
                     user_login: String,
                     user_pass: String,
                     user_phone: String,
                     otp: String) {
        webService.register(user_login, user_pass, user_phone, otp).enqueue(object : Callback<RegisterUserPojo> {
            override fun onResponse(call: Call<RegisterUserPojo>?, response: Response<RegisterUserPojo>?) {
                response?.body()?.let {
                    successHandler(it)
                }
                if (response?.code() == 422) {

                    response.errorBody()?.let {
                        val error = ApiHelper.handleAuthenticationError(response.errorBody()!!)
                        failureHandler(error)
                    }

                } else {
                    response?.errorBody()?.let {
                        val error = ApiHelper.handleApiError(response.errorBody()!!)
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<RegisterUserPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }


    fun verifyOtp(successHandler: (GetOtpPojo) -> Unit,
                  failureHandler: (String) -> Unit,
                  user_phone: String) {
        webService.getOtp(user_phone).enqueue(object : Callback<GetOtpPojo> {
            override fun onResponse(call: Call<GetOtpPojo>?, response: Response<GetOtpPojo>?) {
                response?.body()?.let {
                    successHandler(it)
                }
                if (response?.code() == 422) {
                    response.errorBody()?.let {
                        val error = ApiHelper.handleAuthenticationError(response.errorBody()!!)
                        failureHandler(error)
                    }

                } else {
                    response?.errorBody()?.let {
                        val error = ApiHelper.handleApiError(response.errorBody()!!)
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<GetOtpPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }


}