package com.project.sherby.sherby.ui.userProfile.settings

import Preferences
import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.project.sherby.sherby.R
import com.project.sherby.sherby.activity.LauncherActivity
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.terms.FragmentTermsConditions
import com.project.sherby.sherby.ui.userProfile.UserProfileFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.clearValue
import kotlinx.android.synthetic.main.custom_dialog.view.*
import kotlinx.android.synthetic.main.dialog_change_password.view.*
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.map_dialog.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.*
import java.util.*


/**
 * Created by ${Shubham0} on 6/7/2018.
 */


class SettingsFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback {


    private lateinit var mMap: GoogleMap
    private val mDefaultLocation = LatLng(-33.8523341, 151.2106085)
    private val DEFAULT_ZOOM = 15
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private val map = HashMap<String, RequestBody>()
    private var selectedImagePath: String? = null
    private val CAMERA_REQUEST = 0
    private val GALLERY_PICTURE = 1
    private val imagesUriArrayList: ArrayList<Uri> = ArrayList()
    private var file: File? = null
    private var mEditProfileModel: EditProfileModel? = null
    private var city: String = ""
    private var country: String = ""
    private var completeAddress: String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListeners()
        setUpData()
    }


    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!

        mMap.setOnCameraIdleListener {

            val midLatLng = mMap.cameraPosition.target
            Toast.makeText(activity, midLatLng.latitude.toString(), Toast.LENGTH_SHORT).show()
            getAddressFromLatLong(midLatLng)

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mEditProfileModel = ViewModelProviders.of(this).get(EditProfileModel::class.java)
        attachObservers()
    }

    private fun attachObservers() {

        mEditProfileModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = it.message
                map.clear()
            }
        })
        mEditProfileModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mEditProfileModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })



        mEditProfileModel?.responseChangePAssword?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = it.message
                if (message != null) {
                    showSnackBar(message)
                }
            }
        })
        mEditProfileModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mEditProfileModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })


    }

//firstName, lastName, nickName, userId, address, email, displayName, password, image

    override fun onClick(v: View?) {
        when (v) {
            tvBack -> {
                activity?.onBackPressed()
            }
            tvLogOut -> {
                Preferences.prefs!!.clearValue(Constants.IS_LOGGED_IN)
                Preferences.prefs?.clearValue(Constants.USER_ID)
                Toast.makeText(requireContext(), "Logged out successfully", Toast.LENGTH_SHORT).show()
                startActivity(Intent(activity, LauncherActivity::class.java))
            }

            rlPhone -> {
                // openDialog("Phone Number", tvPhone,)
            }
            rlProfilePic -> {
                startDialog()
            }

            rlEmail -> {
                openDialog("Email", tvEmail, "email")
            }

            rlLocation -> {
                openMapFragment()
            }

            rlName -> {
                openDialog("Name", tvName, "displayName")
            }

            rlPassword -> {
                openPasswordDialog("password", tvNewPassword)
            }

            rlTerms -> {
                val intent = Intent(requireContext(), FragmentTermsConditions::class.java)
                intent.putExtra(Constants.VALUE_FOR_TEXT_FILE, 2)
                startActivity(intent)
            }

            rlPrivacyPolicy -> {
                val intent = Intent(requireContext(), FragmentTermsConditions::class.java)
                intent.putExtra(Constants.VALUE_FOR_TEXT_FILE, 3)
                startActivity(intent)
            }

            rlHelp -> {
                val intent = Intent(requireContext(), FragmentTermsConditions::class.java)
                intent.putExtra(Constants.VALUE_FOR_TEXT_FILE, 1)
                startActivity(intent)
            }


        }
    }

    private fun openMapFragment() {
        val dialog = Dialog(activity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.map_dialog)
        dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        // dialog.window.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

//        dialog.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_Panel)
//        dialog.st
        dialog.show()

        MapsInitializer.initialize(activity!!)
        val mMapView: MapView? = dialog.findViewById(R.id.mapView)
        mMapView!!.onCreate(dialog.onSaveInstanceState())
        mMapView.getMapAsync(this)
        mMapView.onResume()// needed to get the map to display immediately
        dialog.tvSetLocation.setOnClickListener {
            tvNewLocation.text = city
            map["address"] = RequestBody.create(MediaType.parse("multipart/form-data"),
                    completeAddress)
            dialog.dismiss()
            setData()
        }
        fetchCurrenLocation()
    }

    @SuppressLint("MissingPermission")
    private fun fetchCurrenLocation() {
        fusedLocationClient!!.lastLocation
                .addOnSuccessListener { location: Location? ->
                    //                        mMap.addMarker(MarkerOptions().position(LatLng(location!!.latitude,location.longitude)).title("Current location"))
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location!!.latitude, location.longitude), DEFAULT_ZOOM.toFloat()))
                }
    }

    private fun startDialog() {
        val myAlertDialog = AlertDialog.Builder(
                requireContext())
        myAlertDialog.setTitle("Upload Pictures Option")
        myAlertDialog.setMessage("How do you want to set your picture?")

        myAlertDialog.setPositiveButton("Gallery"
        ) { arg0, arg1 ->
            val intent = Intent()
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.action = Intent.ACTION_PICK
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICTURE)
        }

        myAlertDialog.setNegativeButton("Camera"
        ) { arg0, arg1 ->
            val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }
        myAlertDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        selectedImagePath = null


        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == CAMERA_REQUEST) {
            val encodedImage: String
            val photo: Bitmap = data!!.extras.get("data") as Bitmap
            encodedImage = encodeImage(photo)
            map["image"] = RequestBody.create(MediaType.parse("multipart/form-data"),
                    encodedImage)
            setData()
        } else if (resultCode == AppCompatActivity.RESULT_OK && requestCode == GALLERY_PICTURE) {

            var imageStream: InputStream? = null
            try {
                if (data != null) {
                    imageStream = activity!!.contentResolver.openInputStream(data.data)
                }
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

            val yourSelectedImage = BitmapFactory.decodeStream(imageStream)

            val encodedImage: String = encodeImage(yourSelectedImage)

            map["image"] = RequestBody.create(MediaType.parse("multipart/form-data"),
                    encodedImage)
            setData()
        }
    }


    private fun encodeImage(bm: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 40, baos)
        val b = baos.toByteArray()

        return android.util.Base64.encodeToString(b, android.util.Base64.DEFAULT)

    }

    private fun encodeImage(path: String): String {

        val imagefile = File(path)
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(imagefile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        val bm = BitmapFactory.decodeStream(fis)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
//Base64.de
        return android.util.Base64.encodeToString(b, android.util.Base64.DEFAULT)

    }

    private fun openDialog(name: String, textView: TextView, mapValue: String) {
        val dialogBuilder = AlertDialog.Builder(requireContext()
                , android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_dialog, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()

        dialogView.tvToolbarDialog.text = "Change $name"
        dialogView.editTextName.hint = "Enter $name"


        val editText = dialogView.findViewById<View>(R.id.editTextName) as EditText

        dialogView.tvDialogSubmit.setOnClickListener {
            textView.text = editText.text.toString()
            map[mapValue] = RequestBody.create(MediaType.parse("multipart/form-data"),
                    editText.text.toString())
            setData()
            b.dismiss()
        }


        b.show()
    }

    private fun openPasswordDialog(name: String, textView: TextView) {
        val dialogBuilder = AlertDialog.Builder(requireContext(), android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_change_password, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()
        val editText = dialogView.findViewById<View>(R.id.etOldPassword) as EditText
        val editTextNew = dialogView.findViewById<View>(R.id.etNewPassword) as EditText

        dialogView.tvToolbarDialogPassword.text = "Change Password"

        dialogBuilder.setTitle(name)
        dialogBuilder.setMessage("Enter you new $name Below")
        dialogView.tvChangePassword.setOnClickListener {
            mEditProfileModel?.changePassword(Preferences.prefs!!.getString(Constants.USER_ID, "0")
                    , editText.text.toString(), editTextNew.text.toString().trim())
            b.dismiss()
        }


        b.show()
    }


    private fun setUpData() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        tvPhone.text = Preferences.prefs!!.getString(Constants.PHONE, " ")
        tvEmail.text = Preferences.prefs!!.getString(Constants.USER_EMAIL, "Email")
    }

    private fun setData() {


        map["userId"] = RequestBody.create(MediaType.parse("multipart/form-data"),
                Preferences.prefs!!.getString(Constants.USER_ID, "0"))

        mEditProfileModel?.editProfile(map)


    }

    private fun clickListeners() {
        tvBack.setOnClickListener(this)
        tvLogOut.setOnClickListener(this)
        rlPhone.setOnClickListener(this)
        rlEmail.setOnClickListener(this)
        rlProfilePic.setOnClickListener(this)
        rlLocation.setOnClickListener(this)
        rlName.setOnClickListener(this)
        rlPassword.setOnClickListener(this)
        rlTerms.setOnClickListener(this)
        rlHelp.setOnClickListener(this)
        rlPrivacyPolicy.setOnClickListener(this)
    }


    private fun getAddressFromLatLong(midLatLng: LatLng) {

        val geocoder = Geocoder(activity, Locale.getDefault())
        val addresses: MutableList<Address>? = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1)
        addresses!![0].getAddressLine(1)
        city = addresses[0].adminArea
        country = addresses[0].countryName
        completeAddress = addresses[0].getAddressLine(0)

    }

}