package com.project.sherby.sherby.utils

/**
 * Created by android on 2/11/17.
 */

object Constants {


    const val BASE_URL = "http://webappsitesdemo.com/sherby/RestApi/"
    const val FINAL_BASE_URL = "https://sherbby.com/RestApi/"
    const val Listing = "listing.php"
    const val REGISTER = "register.php"
    const val PICK_PACKAGE = "listingPackages.php"
    const val LOGIN = "login.php"
    const val SINGLE_LISTING = "single-listing.php"
    const val PRODUCT_ID = "product_id"
    const val LATITUDE = "lat"
    const val LONGITUDE = "lon"
    const val CATEGORY = "category.php"
    const val USER_PROFILE = "getProfile.php"
    const val POST_ITEM = "createListing.php"
    const val EDIT_PROFILE = "EditProfile.php"
    const val POST_ID = "post_id"
    const val SEND_MESSAGE = "sendMessage.php"
    const val CHAT_HISTORY = "chatWindow.php"
    const val RECEIVED_MESSAGES = "recieveMessgae.php"
    const val ADD_FAVOURITES = "addfavourites.php"
    const val FAVOURITES = "favourites"
    const val FETCH_FAVOURITES = "listfavourites.php"
    const val USER_EMAIL = "email"
    const val LOCATION = "location"
    const val PHONE = "phone"
    const val VERIFY = "verify.php"
    const val MY_LISTINGS = "mylistings.php"
    const val REPORT = "addreport.php"
    const val FILTER = "Filter.php"
    const val CATEGORY_ID = "cat_id"
    const val GET_OTP = "getOtp.php"
    const val MAKE_AN_OFFER = "makeOffer.php"
    const val CHANGE_PASSWORD = "changePassword.php"
    const val IS_LOGGED_IN = "legged_in"
    const val NOTIFICATION = "NotificationTab.php"
    const val FORGOT_PASSWORD = "lost-password.php"
    const val TOKEN = "firebase_token"
    const val FETCH_COUNTRIES = "getCountryList.php"
    const val DELETE_POST = "DeleteListing.php"
    const val VALUE_FOR_TEXT_FILE = "value_text_file"
    const val PUBLISH_UNPUBLISH = "publishUnpublish.php"
    const val SOLD_UNSOLD = "EditListing.php"
    const val RATE_REVIEW = "rateReview.php"


    const val PERMISSION_REQUEST_CODE = 98
    const val LOCATION_PERMISSION_REQUEST_CODE = 101
    const val HANDLER_DELAY_TIME: Long = 2000

    internal interface httpcodes {
        companion object {
            val STATUS_OK = 200
            val STATUS_BAD_REQUEST = 400
            val STATUS_SESSION_EXPIRED = 401
            val STATUS_PLAN_EXPIRED = 403
            val STATUS_VALIDATION_ERROR = 404
            val STATUS_SERVER_ERROR = 500
            val STATUS_UNKNOWN_ERROR = 503
            val STATUS_API_VALIDATION_ERROR = 422

        }
    }

    const val SNACK_BAR_DURATION = 2000

    //FAILURE MESSAGES
    const val SOMETHING_WENT_WRONG = "Quelque chose c'est mal passé. Merci d'essayer plus tard!"
    const val FAILURE_TIME_OUT_ERROR = "Demander un temps mort!"
    const val FAILURE_SOMETHING_WENT_WRONG = "Quelque chose c'est mal passé. Merci d'essayer plus tard!"
    const val FAILURE_SERVER_NOT_RESPONDING = "Oops! on dirait que nous avons des problèmes internes. Veuillez réessayer plus tard."
    const val FAILURE_INTERNET_CONNECTION = "La connexion Internet semble être hors ligne. Veuillez vérifier vos paramètres réseau."
    const val USER_ID = "user_id"
    const val USER_NAME = "user_name"
    const val BUNDLE = "bundle"


    enum class TYPE(val type: Int) {
        SIMPLE(0), BREAKFAST(1), LUNCH(2), TASK(3), UNVERIFIEDOCTOR(4)
    }

    enum class WEEK(val type: Int) {
        MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6)
    }


}