package com.project.sherby.sherby.data.receivedBidMessages


import com.google.gson.annotations.SerializedName


class Chat {

    @SerializedName("id")
    var id: Long? = null
    @SerializedName("message_content")
    var messageContent: String? = null
    @SerializedName("message_title")
    var messageTitle: String? = null
    @SerializedName("reciever_id")
    var recieverId: String? = null
    @SerializedName("reciever_name")
    var recieverName: String? = null
    @SerializedName("sender_id")
    var senderId: String? = null
    @SerializedName("sender_name")
    var senderName: String? = null
    @SerializedName("time")
    var time: String? = null

}
