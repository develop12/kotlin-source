package com.project.sherby.sherby.ui.rateReview

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.Validations
import kotlinx.android.synthetic.main.fragment_rate_review.*


/**
 * Created by ${Shubham} on 9/18/2018.
 */
class RatingsFragment : com.project.sherby.sherby.other.BaseFragment() {


    private var mViewModel: RateReviewModel? = null
    private var rating: String = "2.5"
    private var validations:Validations?=null
    private var postId:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel = ViewModelProviders.of(this).get(RateReviewModel::class.java)
        attachObservers()
    }

    private fun attachObservers() {
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {
                    showLoading(false)
                    activity?.onBackPressed()
                } else
                    showSnackBar(message)

            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {  it ->
            it?.let { showLoading(it) }
        })
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBundledData()
        clickListeners()
    }

    private fun clickListeners() {
        tvBackReview.setOnClickListener {
            activity?.onBackPressed()
        }

        tvSubmitReview.setOnClickListener {
            if (Validations.isEmpty(etAdReview)) {
                mViewModel?.ratePost(Preferences.prefs?.getString(Constants.USER_ID, "0")!!, postId!!, rating, etAdReview.text.toString())
            }
        }

        ratingBar2.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            this.rating = "$rating"
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rate_review, container, false)
    }


    private fun getBundledData() {
        val bundle = arguments
        if (bundle != null) {
            postId = bundle.getString(Constants.POST_ID)
        }
    }


}