package com.project.sherby.sherby.ui.postItem

import Preferences
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.project.sherby.sherby.R
import com.project.sherby.sherby.activity.CategoryFetchModel
import com.project.sherby.sherby.adapter.AdapterPostedImages
import com.project.sherby.sherby.data.countries.Country
import com.project.sherby.sherby.data.home.Category
import com.project.sherby.sherby.data.pickPackage.Package
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import com.tangxiaolv.telegramgallery.GalleryActivity
import com.tangxiaolv.telegramgallery.GalleryConfig
import kotlinx.android.synthetic.main.fragment_post_item.*
import kotlinx.android.synthetic.main.item_pack_layout.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList


class PostItemFragment : BaseFragment(), View.OnClickListener, RecyclerViewClickListener {

    override fun onClick(view: View, position: Int, url: String) {

    }


    private var countryId: String? = null
    private var value:Int?=0
    private var itemPackages: ArrayList<Package>? = ArrayList()
    private var selectedImagePath: String? = null
    private var arrayOfBase: ArrayList<String?> = ArrayList()
    private val CAMERA_REQUEST = 0
    private val GALLERY_PICTURE = 420
    private val imagesUriArrayList: ArrayList<Uri> = ArrayList()
    private var mViewModelCategory: CategoryFetchModel? = null
    private var mViewModel: PostItemModel? = null
    private var items: ArrayList<Category> = ArrayList()
    private var countries: ArrayList<Country> = ArrayList()
    private var strName: String = ""
    private var position: Int = 0
    private lateinit var mMap: GoogleMap
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var city: String = ""
    private var country: String = ""
    private var completeAddress: String = ""
    private var pack: String = ""
    private var lat: Double = 0.0
    private var lng: Double = 0.0


    override fun onClick(v: View?) {
        when (v) {
            tvCreatePost -> {


                mViewModel?.postItem(etPostTitle.text.toString(),
                        etDescription.text.toString().trim(),
                        etPostPrice.text.toString().trim(),
                        etCity.text.toString(),
                        countryId!!,
                        etContactNumber.text.toString().trim(),
                        items[position].id.toString(),
                        etPostSpecifications.text.toString().trim(),
                        Preferences.prefs?.getString(Constants.USER_ID, "0")!!,
                        arrayOfBase,
                        pack,
                        "active"
                )
            }

            ivPostImageOne -> {
                startDialog()
            }

            etPostCategory -> {
                categoryListDialog()
            }

            etCountry -> {
                countryListDialog()
            }
        }
    }


    private fun categoryListDialog() {

        val builderSingle = AlertDialog.Builder(requireContext())
        val arrayAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.select_dialog_singlechoice)
        for (i in 0 until items.size) {
            arrayAdapter.add(items[i].name)
        }

        builderSingle.setAdapter(arrayAdapter) { dialog, which ->
            strName = arrayAdapter.getItem(which)
            etPostCategory.text = strName
            position = which
            etPostCategory.setTextColor(Color.BLACK)
        }
        builderSingle.show()
    }

    private fun countryListDialog() {

        val builderSingle = AlertDialog.Builder(requireContext())
        val arrayAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.select_dialog_singlechoice)
        for (i in 0 until countries.size) {
            arrayAdapter.add(countries[i].name)
        }

        builderSingle.setAdapter(arrayAdapter) { dialog, which ->
            strName = arrayAdapter.getItem(which)
            etCountry.text = strName
            position = which
            countryId = countries[position].id.toString()
            Toast.makeText(requireContext(), countries[position].id.toString(), Toast.LENGTH_SHORT).show()

            etCountry.setTextColor(Color.BLACK)
        }
        builderSingle.show()
    }

    private fun startDialog() {
        val myAlertDialog = AlertDialog.Builder(
                requireContext())
        myAlertDialog.setTitle("Upload Pictures Option")
        myAlertDialog.setMessage("How do you want to set your picture?")

        myAlertDialog.setPositiveButton("Gallery"
        ) { arg0, arg1 ->
//            val intent = Intent()
//            intent.type = "image/*"
//            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
//            intent.action = Intent.ACTION_GET_CONTENT
//            startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_PICTURE)
            val config: GalleryConfig? = GalleryConfig.Build()
                    .limitPickPhoto(value!!)
                    .singlePhoto(false)
                    .hintOfPick("this is pick hint")
                    .filterMimeTypes(arrayOf("image/jpeg"))
                    .build()
            GalleryActivity.openActivity(activity, GALLERY_PICTURE, config)

        }

        myAlertDialog.setNegativeButton("Camera"
        ) { arg0, arg1 ->
            val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }
        myAlertDialog.show()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(PostItemModel::class.java)
        mViewModelCategory = ViewModelProviders.of(this).get(CategoryFetchModel::class.java)
        attachObservers()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        fetchCategoriesApi()
        clickListeners()
        fetchPackages()
        fetchCountriesApi()
    }

    private fun fetchCountriesApi() {
        mViewModelCategory?.fetchCountries()
    }

    private fun fetchPackages() {
        mViewModelCategory?.fetchPackages()
    }

    private fun fetchCategoriesApi() {
        mViewModelCategory?.fetchCategories()
    }

    private fun clickListeners() {
        etPostCategory.setOnClickListener(this)
        ivPostImageOne.setOnClickListener(this)
        tvCreatePost.setOnClickListener(this)
        etCountry.setOnClickListener(this)

        radioGroup2.setOnCheckedChangeListener { radioGroup: RadioGroup, i: Int ->

            if (i == R.id.rbPack1) {
                value= 3
                include.visibility = View.VISIBLE
                tvTotalNoOfPictures.text = itemPackages!![0].images_number
                tvDays.text = itemPackages!![0].activation_period
                tvBumpsUp.text = itemPackages!![0].description
                tvPriceOfPack.text = itemPackages!![0].price
                tvPackId.text = itemPackages!![0].name
            }
            if (i == R.id.rbPack2) {
                value= 5
                include.visibility = View.VISIBLE
                tvTotalNoOfPictures.text = itemPackages!![1].images_number
                tvDays.text = itemPackages!![1].activation_period
                tvBumpsUp.text = itemPackages!![1].description
                tvPriceOfPack.text = itemPackages!![1].price
                tvPackId.text = itemPackages!![1].name
            }
            if (i == R.id.rbPack3) {
                value= 10
                include.visibility = View.VISIBLE
                tvTotalNoOfPictures.text = itemPackages!![2].images_number
                tvDays.text = itemPackages!![2].activation_period
                tvBumpsUp.text = itemPackages!![2].description
                tvPriceOfPack.text = itemPackages!![2].price
                tvPackId.text = itemPackages!![2].name
            }

        }
    }

    private fun attachObservers() {
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it.message!!)
            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let { showLoading(it) }
        })


        //categories api

        mViewModelCategory?.response!!.observe(this, android.arch.lifecycle.Observer {
            it?.let {

                items = it as ArrayList<Category>

            }
        })

        mViewModelCategory?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModelCategory?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })

        //fetch Packages

        mViewModelCategory?.responsePakages!!.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                if (it.success == 1) {
                    itemPackages = it.packages as ArrayList<Package>
                } else {
                    showSnackBar(it.message!!)
                }
            }
        })

        mViewModelCategory?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModelCategory?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })

        //fetch countries

        mViewModelCategory?.responseCountries?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                if (it.success == 1) {
                    countries = it.countries as ArrayList<Country>
                }
            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let { showLoading(it) }
        })


    }

    private fun setUpRecyclerView() {

//        rvPackageRecycler.layoutManager = LinearLayoutManager(activity, LinearLayout.HORIZONTAL, false)
//        rvPackageRecycler.adapter = AdapterPickYourPack(itemPackages!!, requireContext(), this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        selectedImagePath = null
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == CAMERA_REQUEST) {
            val encodedImage: String
            val photo: Bitmap = data!!.extras.get("data") as Bitmap

            arrayOfBase.clear()
            encodedImage = encodeImage(photo)
            arrayOfBase.add(0, encodedImage)

            getImageUri(requireContext(), photo)

            imagesUriArrayList.clear()
            imagesUriArrayList.add(getImageUri(requireContext(), photo))

            setUpPostedItems()
        } else if (resultCode == AppCompatActivity.RESULT_OK && requestCode == GALLERY_PICTURE) {
            imagesUriArrayList.clear()
//            if (data != null) {
//                if (null != data.clipData) {
//                    if (data.clipData!!.itemCount > 0) {
//                        for (i in 0 until data.clipData!!.itemCount) {
//                            imagesUriArrayList.add(data.clipData?.getItemAt(i)?.uri!!)
//                        }
//                    } else
//                        imagesUriArrayList.add(data.clipData?.getItemAt(0)?.uri!!)
//                } else {
//                    imagesUriArrayList.add(data.data)
//                }
//            }

            var photos:ArrayList<String> = data!!.getSerializableExtra(GalleryActivity.PHOTOS) as ArrayList<String>
            for (i in 0 until photos.size) {
                imagesUriArrayList.add(Uri.fromFile(File(photos[i])))
            }
            setData(imagesUriArrayList)
        }
    }

    private fun setUpPostedItems() {
        ivPostImageOne.visibility = View.INVISIBLE
        rvTotalImages.visibility = View.VISIBLE
        rvTotalImages.layoutManager = StaggeredGridLayoutManager(1, 0)
        rvTotalImages.adapter = AdapterPostedImages(imagesUriArrayList, requireContext())

    }

    private fun setData(imagesUriArrayList: ArrayList<Uri>) {

        var imageStream: InputStream?
        var encodedImage: String

        for (i in 0 until imagesUriArrayList.size) {

            imageStream = activity!!.contentResolver.openInputStream(imagesUriArrayList[i])
            val yourSelectedImage = BitmapFactory.decodeStream(imageStream)
            encodedImage = encodeImage(yourSelectedImage)
            arrayOfBase.add(i, encodedImage)

        }
        setUpPostedItems()
    }


    private fun encodeImage(bm: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos)
        val b = baos.toByteArray()

        return android.util.Base64.encodeToString(b, android.util.Base64.DEFAULT)

    }

    private fun getAddressFromLatLong(midLatLng: LatLng) {

        val geocoder = Geocoder(activity, Locale.getDefault())
        val addresses: MutableList<Address>? = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1)
        addresses!![0].getAddressLine(1)
        city = addresses[0].adminArea
        country = addresses[0].countryName
        completeAddress = addresses[0].getAddressLine(0)

    }
}
