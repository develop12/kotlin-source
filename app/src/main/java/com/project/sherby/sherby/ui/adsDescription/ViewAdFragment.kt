package com.project.sherby.sherby.ui.adsDescription

import Preferences
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.adapter.AdapterRatings
import com.project.sherby.sherby.adapter.MyCustomPagerAdapter
import com.project.sherby.sherby.data.singlelisting.Rating
import com.project.sherby.sherby.data.singlelisting.SingleListingPojo
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.rateReview.RatingsFragment
import com.project.sherby.sherby.ui.chats.chatsScreen.ActivityChatScreen
import com.project.sherby.sherby.ui.login.LoginActivity
import com.project.sherby.sherby.ui.makeAnOffer.FragmentMakeAnOffer
import com.project.sherby.sherby.ui.report.ReportPostFragment
import com.project.sherby.sherby.utils.Constants
import kotlinx.android.synthetic.main.fragment_ads_detail.*
import kotlinx.android.synthetic.main.included_modify_status.*


/**
 * Created by ${Shubham} on 6/5/2018.
 */
class ViewAdFragment : BaseFragment(), View.OnClickListener {

    private var mViewModel: AdsDescriptionModel? = null
    private var listReview: ArrayList<Rating> = ArrayList()

    private var description: String = ""
    private var specification: String = ""
    private var postId: String = ""
    private var fav: Boolean? = null
    private var postedBy: String? = null
    private var url: String? = " "

    private var publish: String? = null
    private var sold: String? = null

    private var lat: String = ""
    private var lon: String = ""
    private var userId: String = "anonymous"

    private var p_id: String = ""
    private var phone_number = ""
    private val args = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(AdsDescriptionModel::class.java)
        attachObservers()

    }


    override fun onClick(v: View?) {
        when (v) {
            tvDescription -> {
                rvReviewNRatings.visibility = View.GONE
                tvText.visibility=View.VISIBLE
                tvDescription.setBackgroundColor(resources.getColor(R.color.themeColor))
                tvDescription.setTextColor(Color.WHITE)

                tvSpecifications.setBackgroundColor(Color.WHITE)
                tvSpecifications.setTextColor(resources.getColor(R.color.themeColor))

                tvText.text = description

                tvReviews.setBackgroundColor(Color.WHITE)
                tvReviews.setTextColor(resources.getColor(R.color.themeColor))


            }
            tvSpecifications -> {
                rvReviewNRatings.visibility = View.GONE
                tvText.visibility=View.VISIBLE

                tvSpecifications.setBackgroundColor(resources.getColor(R.color.themeColor))
                tvSpecifications.setTextColor(Color.WHITE)

                tvDescription.setBackgroundColor(Color.WHITE)
                tvDescription.setTextColor(resources.getColor(R.color.themeColor))

                tvText.text = specification


                tvReviews.setBackgroundColor(Color.WHITE)
                tvReviews.setTextColor(resources.getColor(R.color.themeColor))

            }

            tvReviews -> {
                tvText.visibility=View.GONE

                tvDescription.setBackgroundColor(Color.WHITE)
                tvDescription.setTextColor(resources.getColor(R.color.themeColor))

                tvSpecifications.setBackgroundColor(Color.WHITE)
                tvSpecifications.setTextColor(resources.getColor(R.color.themeColor))


                tvReviews.setBackgroundColor(resources.getColor(R.color.themeColor))
                tvReviews.setTextColor(Color.WHITE)

                rvReviewNRatings.layoutManager=LinearLayoutManager(requireContext(),RecyclerView.VERTICAL,false)
                rvReviewNRatings.adapter=AdapterRatings(listReview,requireContext())

                rvReviewNRatings.visibility = View.VISIBLE
            }

            ivCallUser -> {

                val callIntent = Intent(Intent.ACTION_DIAL)
                callIntent.data = Uri.parse("tel:" + Uri.encode(phone_number.trim()))
                callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(callIntent)

            }

            ivViewOnMap -> {
                val viewUserLocationFragment = ViewUserLocationFragment()
                val bundle = Bundle()

                bundle.putString(Constants.LATITUDE, lat)
                bundle.putString(Constants.LONGITUDE, lon)

                viewUserLocationFragment.arguments = bundle

                addFragment(viewUserLocationFragment, true, R.id.fragment_container)
                //  bundle.clear()
            }

            ivAdsComment -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                } else {
                    val bundle = Bundle()
                    bundle.putString(Constants.USER_NAME, tvUserName.text.toString())
                    bundle.putString(Constants.POST_ID, postId)
                    val intent = Intent(activity, ActivityChatScreen::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }

            ivFavourites -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                } else {
                    if (!fav!!)
                        ivFavourites.setBackgroundColor(resources.getColor(R.color.favrouteTint))
                    else
                        ivFavourites.setImageDrawable(resources.getDrawable(R.mipmap.favorite))

                    mViewModel?.addFavourites(postId, Preferences.prefs?.getString(Constants.USER_ID, "0")!!)
                }
            }

            tvReport -> {


                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                } else {

                    val bundle = Bundle()
                    bundle.putString(Constants.USER_NAME, tvUserName.text.toString())
                    bundle.putString(Constants.POST_ID, postId)
                    val reportPostFragment = ReportPostFragment()
                    reportPostFragment.arguments = bundle
                    addFragment(reportPostFragment
                            , true, R.id.fragment_container)
                }


            }

            tvMakeAnOffer -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                } else {
                    val bundle = Bundle()
                    bundle.putString(Constants.POST_ID, postId)
                    val fragmentMakeAnOffer = FragmentMakeAnOffer()
                    fragmentMakeAnOffer.arguments = bundle
                    addFragment(fragmentMakeAnOffer
                            , true, R.id.fragment_container)
                }
            }

            tvDelete -> {
                mViewModel?.deletePost(p_id)
            }

            tvUnpublish -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                } else {
                    if (publish == "0") {
                        mViewModel?.unpublish(p_id, "1")
                    } else
                        mViewModel?.unpublish(p_id, "0")
                }
            }

            ivShareAds -> {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Please click link to view Ads image!" +
                        "\n" + url)
                startActivity(Intent.createChooser(shareIntent, url))
            }


            tvUnSold -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                } else {
                    if (sold == "0") {
                        mViewModel?.unsold(p_id, Preferences.prefs?.getString(Constants.USER_ID, "0")!!,"1")
                    } else
                        mViewModel?.unsold(p_id, Preferences.prefs?.getString(Constants.USER_ID, "0")!!,"0")
                }
            }

            ivReviewRatings -> {
                if (!Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                    startActivity(Intent(activity, LoginActivity::class.java))
                }
                else
                {
                    val bundle = Bundle()
                    bundle.putString(Constants.POST_ID, postId)

                    val ratingsFragment = RatingsFragment()
                    ratingsFragment.arguments=bundle
                    addFragment(ratingsFragment,true,R.id.fragment_container)
                }
            }



        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ads_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBundledArguments()
        clickListeners()
    }

    private fun getBundledArguments() {

        val bundle = arguments

        if (bundle != null) {
            p_id = bundle.getString(Constants.PRODUCT_ID)
            if (Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)!!) {
                userId = Preferences.prefs?.getString(Constants.USER_ID, "0")!!
            }
            mViewModel?.fetchAdsInfo(p_id, userId)
        }

    }


    private fun clickListeners() {
        tvDescription.setOnClickListener(this)
        tvSpecifications.setOnClickListener(this)

        ivCallUser.setOnClickListener(this)
        ivViewOnMap.setOnClickListener(this)

        ivAdsComment.setOnClickListener(this)
        ivFavourites.setOnClickListener(this)

        tvReport.setOnClickListener(this)
        tvMakeAnOffer.setOnClickListener(this)

        tvUnSold.setOnClickListener(this)
        tvUnpublish.setOnClickListener(this)

        tvDelete.setOnClickListener(this)
        ivShareAds.setOnClickListener(this)

        ivReviewRatings.setOnClickListener(this)
        tvReviews.setOnClickListener(this)
    }

    private fun attachObservers() {



        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {
                    setData(it)
                } else
                    showSnackBar(message) }

        })

        //add favourites
        mViewModel?.responseAddToFavourites?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = "${it.message}"
                showSnackBar(message)
            }
        })

        //deletePost
        mViewModel?.responseDelete?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {
                    activity?.onBackPressed()
                } else
                    showSnackBar(message)
            }
        })



        mViewModel?.responseDelete?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {
                    activity?.onBackPressed()
                } else
                    showSnackBar(message)
            }
        })


        mViewModel?.responseUnpublish?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {
                    if (publish == "0") {
                        tvUnpublish.text = resources.getString(R.string.unpublish)
                        publish = "1"
                    } else {
                        tvUnpublish.text = resources.getString(R.string.publish)
                        publish = "0"
                    }
                } else
                    showSnackBar(message)
            }
        })


        mViewModel?.responseUnsold?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let {
                val message = "${it.message}"
                if (it.success == 1) {
                    if (sold == "0") {
                        tvUnSold.text =resources.getString(R.string.unsold)
                        sold = "1"
                    } else {
                        tvUnSold.text = resources.getString(R.string.sold)
                        sold = "0"
                    }
                } else
                    showSnackBar(message)
            }
        })


        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer { it ->
            it?.let { showLoading(it) }
        })

    }

    private fun setData(it: SingleListingPojo) {
        // if(Preferences.prefs?.getString(Constants.USER_ID, "0")==it.post_data.a)
        if(it.post_data?.images?.size!=0) {
            url = it.post_data?.images?.get(0)
        }
        sold = it.post_data?.sold_status
        publish = it.post_data?.status.toString()

        tvAdName.text = it.post_data?.title
        tvAdPrice.text = it.post_data?.price
        tvTotalViews.text = it.post_data?.views + " views"
        tvText.text = it.post_data?.description
        tvUserName.text = it.post_data?.userName
        ivUserImage.loadImage(it.post_data?.userImage!!)

        description = it.post_data?.description!!
        specification = it.post_data?.specification!!
        phone_number = it.post_data?.mobile!!
        lat = it.post_data?.latitude!!
        lon = it.post_data?.longitude!!
        postId = it.post_data?.postId!!
        fav = it.post_data?.inFav

        if (it.post_data?.inFav!!) {
            ivFavourites.setColorFilter(ContextCompat.getColor(requireContext(), R.color.favrouteTint),
                    android.graphics.PorterDuff.Mode.MULTIPLY)
        }

        if (Preferences.prefs?.getString(Constants.USER_ID, "0") == it.post_data?.authorId) {

            tvMakeAnOffer.isClickable = false
            ivAdsComment.isClickable = false
            ivReviewRatings.isClickable = false
            included_modify.visibility = View.VISIBLE

            tvSoldBy.visibility = View.GONE

            ivUserImage.visibility = View.GONE
            tvUserName.visibility = View.GONE
            ivCallUser.visibility = View.GONE
            ivViewOnMap.visibility = View.GONE

        }

        if (it.post_data?.bid_placed!!) {
            tvMakeAnOffer.isClickable = false
//            showSnackBar("Bid already placed")
        }


        if (it.post_data?.sold_status == "0") {
            tvUnSold.text = resources.getString(R.string.sold)
        }

        if (it.post_data?.status == 0) {
            tvUnpublish.text = resources.getString(R.string.publish)
        }

        ivAdsImage.adapter = MyCustomPagerAdapter(requireContext(), it.post_data?.images as ArrayList<String>)
        indicator.setViewPager(ivAdsImage)

        postedBy=it.post_data?.authorId
        listReview = it.post_data?.rating as ArrayList<Rating>
    }

}