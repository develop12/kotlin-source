package com.project.sherby.sherby.utils

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

open class MyViewModel : ViewModel() {

    var apiError = MutableLiveData<String>()
    var onFailure = MutableLiveData<Throwable>()
    var badRequest = MutableLiveData<String>()
    var isLoading = MutableLiveData<Boolean>()
    var isPullToRefreshLoading = MutableLiveData<Boolean>()

}