package com.project.sherby.sherby.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.notifications.NotificationDatum
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.item_notifications.view.*


class AdapterNotifications(private val items: ArrayList<NotificationDatum>, private val context: Context, private var clickListener: RecyclerViewClickListener)
    : RecyclerView.Adapter<AdapterNotifications.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16)).placeholder(R.drawable.ic_person)

        holder.tvName.text = items[position].title
        Glide.with(context).load(items[position].userImage).apply(requestOptions).into(holder.ivImage)
        holder.tvType.text = items[position].message

        holder.init(clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notifications, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvName = view.tvNotificationText!!
        val ivImage = view.ivNotificationImage!!
        var tvType = view.tvNotificationTypes!!


        fun init(click: RecyclerViewClickListener) {
            itemView.setOnClickListener {
                click.onClick(itemView, adapterPosition, tvName.text.toString())
            }
        }
    }
}