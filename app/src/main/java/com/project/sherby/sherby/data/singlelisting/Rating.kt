package com.project.sherby.sherby.data.singlelisting

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Rating : Serializable {

    @SerializedName("userId")
    @Expose
    var userId: String? = null
    @SerializedName("userImage")
    @Expose
    var userImage: String? = null
    @SerializedName("userName")
    @Expose
    var userName: String? = null
    @SerializedName("rating")
    @Expose
    var rating: String? = null
    @SerializedName("review")
    @Expose
    var review: String? = null


}