package com.project.sherby.sherby.ui.adsDescription

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import com.project.sherby.sherby.data.singlelisting.SingleListingPojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object AdsInformationRepository {
    private val webService = ApiHelper.createService()


    fun getTodoData(successHandler: (SingleListingPojo) -> Unit, failureHandler: (String) -> Unit,
                    postId: String, userId: String) {
        webService.singleListing(postId, userId).enqueue(object : Callback<SingleListingPojo> {
            override fun onResponse(call: Call<SingleListingPojo>?, response: Response<SingleListingPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }

            }

            override fun onFailure(call: Call<SingleListingPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }


    fun addFavourites(successHandler: (AddFavouritesPojo) -> Unit, failureHandler: (String) -> Unit, postId: String, userId: String) {
        webService.addFavourites(postId, userId).enqueue(object : Callback<AddFavouritesPojo> {
            override fun onResponse(call: Call<AddFavouritesPojo>?, response: Response<AddFavouritesPojo>?) {
                response?.body()?.let {
                    if (response.code() == 200) {
                        successHandler(it)
                    }
                }

                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<AddFavouritesPojo>?, t: Throwable?) {
                t?.let {
                    failureHandler(ApiFailureTypes().getFailureMessage(it))
                }
            }
        })
    }


    fun deletePost(successHandler: (AddFavouritesPojo) -> Unit, failureHandler: (String) -> Unit, doctorId: String) {

        webService.deletePost(doctorId).enqueue(object : Callback<AddFavouritesPojo> {
            override fun onResponse(call: Call<AddFavouritesPojo>?, response: Response<AddFavouritesPojo>?) {
                response?.body()?.let {

                    successHandler(it)

                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<AddFavouritesPojo>?, t: Throwable?) {
                t?.let {

                    failureHandler(ApiFailureTypes().getFailureMessage(it))

                }
            }
        })
    }


    fun unpublish(successHandler: (AddFavouritesPojo) -> Unit, failureHandler: (String) -> Unit, doctorId: String, userId: String) {

        webService.publishUnpublish(doctorId, userId).enqueue(object : Callback<AddFavouritesPojo> {
            override fun onResponse(call: Call<AddFavouritesPojo>?, response: Response<AddFavouritesPojo>?) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<AddFavouritesPojo>?, t: Throwable?) {
                t?.let {

                    failureHandler(ApiFailureTypes().getFailureMessage(it))

                }
            }
        })
    }



    fun unsold(successHandler: (AddFavouritesPojo) -> Unit, failureHandler: (String) -> Unit, doctorId: String, userId: String, status : String) {

        webService.soldUnsold(doctorId, userId, status).enqueue(object : Callback<AddFavouritesPojo> {
            override fun onResponse(call: Call<AddFavouritesPojo>?, response: Response<AddFavouritesPojo>?) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<AddFavouritesPojo>?, t: Throwable?) {
                t?.let {

                    failureHandler(ApiFailureTypes().getFailureMessage(it))

                }
            }
        })
    }


}