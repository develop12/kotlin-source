package com.project.sherby.sherby.data.chatHistory

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Chat_list : Serializable {

    @SerializedName("user_id")
    @Expose
    var user_id: String? = null
    @SerializedName("user_image")
    @Expose
    var user_image: String? = null
    @SerializedName("user_name")
    @Expose
    var user_name: String? = null
    @SerializedName("list_id")
    @Expose
    var list_id: String? = null
    @SerializedName("list_name")
    @Expose
    var list_name: String? = null
    @SerializedName("time")
    @Expose
    var time: String? = null

}