package com.project.sherby.sherby.data.countries

import java.io.Serializable

class Country : Serializable {

    var id: Int? = null
    var name: String? = null
    }