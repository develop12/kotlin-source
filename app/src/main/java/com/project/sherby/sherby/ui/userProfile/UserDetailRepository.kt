package com.project.sherby.sherby.ui.userProfile

import com.project.sherby.sherby.api.service.ApiHelper
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import com.project.sherby.sherby.data.userProfile.Favourite
import com.project.sherby.sherby.data.userProfile.GetUserProfilePojo
import com.project.sherby.sherby.utils.security.ApiFailureTypes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by android on 29/3/18.
 */
object UserDetailRepository {

    private val webService = ApiHelper.createService()

    fun getUserProfile(successHandler: (GetUserProfilePojo) -> Unit, failureHandler: (String) -> Unit, doctorId: String) {

        webService.userProfile(doctorId).enqueue(object : Callback<GetUserProfilePojo> {
            override fun onResponse(call: Call<GetUserProfilePojo>?, response: Response<GetUserProfilePojo>?) {
                response?.body()?.let {

                    successHandler(it)

                }
                response?.errorBody()?.let {
                    val error = ApiHelper.handleApiError(response.errorBody()!!)
                    failureHandler(error)
                }
            }

            override fun onFailure(call: Call<GetUserProfilePojo>?, t: Throwable?) {
                t?.let {

                    failureHandler(ApiFailureTypes().getFailureMessage(it))

                }
            }
        })
    }








}