package com.project.sherby.sherby.api.firebase

import Preferences
import android.app.NotificationManager
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.saveValue
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import com.project.sherby.sherby.ui.splash.SplashActivity


class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.d(TAG, "From: " + remoteMessage!!.from!!)

        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
        }

        sendNotification(remoteMessage)

    }

    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: " + token!!)
        Preferences.prefs!!.saveValue(Constants.TOKEN, token)
        sendRegistrationToServer(token)
    }


    private fun sendRegistrationToServer(token: String?) {

    }


    private fun sendNotification(messageBody: RemoteMessage?) {
        val notifyID = 1
        val CHANNEL_ID = "my_channel_01"// The id of the channel.

        val notificationIntent = Intent(applicationContext, SplashActivity::class.java)
//**add this line**
        val bundle = Bundle()
        bundle.putString("fromNotif","1")
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
//**edit this line to put requestID as requestCode**
        val contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        notificationIntent.putExtra("fromNotif","1")
        notificationIntent.putExtras(bundle)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.sherbby)
                .setContentTitle("New Notification")
                .setContentText(messageBody!!.data.toString())
                .setContentIntent(contentIntent)
                .setChannelId(CHANNEL_ID).build()

        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(notifyID, notification)

    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}