package com.project.sherby.sherby.ui.filters

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.project.sherby.sherby.R
import com.project.sherby.sherby.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_filters.*

/**
 * Created by ${Shubham} on 5/30/2018.
 */
class FiltersActivity : BaseActivity(), View.OnClickListener {
    override fun iniView(savedInstanceState: Bundle?) {

    }

    override fun onClick(v: View?) {

        when (v) {
            tvChangeLocation -> {
                val intent = Intent(this@FiltersActivity, ChangeLocationActivity::class.java)
                startActivity(intent)
            }

            tvNewestFirst -> {
                sortByColor(this, tvNewestFirst)
            }

            tvClosestFirst -> {
                sortByColor(this, tvClosestFirst)
            }

            tvPriceLowToHigh -> {
                sortByColor(this, tvPriceLowToHigh)
            }

            tvPriceHighToLow -> {
                sortByColor(this, tvPriceHighToLow)
            }

            tvLast24Hour -> {
                postedWithin(this, tvLast24Hour)
            }

            tvLast7Days -> {
                postedWithin(this, tvLast7Days)
            }

            tvLast30Days -> {
                postedWithin(this, tvLast30Days)
            }

            tvAllListings -> {
                postedWithin(this, tvAllListings)
            }

            tvSaveFilters -> {
                onBackPressed()
            }

            tvSaveFilters -> {
                onBackPressed()
            }
        }
    }

    override fun getID(): Int {
        return R.layout.activity_filters
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)
        initViews()
        clickListeners()
    }

    private fun initViews() {
        tvLast24Hour.setTextColor(resources.getColor(R.color.themeColor))
        tvLast24Hour.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check, 0)

        tvNewestFirst.setTextColor(resources.getColor(R.color.themeColor))
        tvNewestFirst.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check, 0)
    }

    private fun clickListeners() {
        tvChangeLocation.setOnClickListener(this)
        tvNewestFirst.setOnClickListener(this)
        tvClosestFirst.setOnClickListener(this)
        tvPriceHighToLow.setOnClickListener(this)
        tvPriceLowToHigh.setOnClickListener(this)

        tvLast24Hour.setOnClickListener(this)
        tvLast7Days.setOnClickListener(this)
        tvLast30Days.setOnClickListener(this)
        tvAllListings.setOnClickListener(this)

        tvSaveFilters.setOnClickListener(this)
        tvDrawerLayout.setOnClickListener(this)
    }
}