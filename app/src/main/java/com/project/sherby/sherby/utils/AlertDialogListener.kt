package com.project.sherby.sherby.utils

interface AlertDialogListener {
    fun actionOk(pos: Int)
}