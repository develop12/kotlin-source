package com.project.sherby.sherby.data

data class Status(val status: Int, val message: String)