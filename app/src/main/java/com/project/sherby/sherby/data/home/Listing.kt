package com.project.sherby.sherby.data.home


import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Listing : Serializable {

    @SerializedName("date")
    var date: String? = null
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("image")
    var image: String? = null
    @SerializedName("location")
    var location: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("price")
    var price: String? = null

}
