package com.project.sherby.sherby.ui.chats

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.chatHistory.ChatHistoryPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by android on 29/3/18.
 */
class ChatHistoryModel : MyViewModel() {

    var response = MutableLiveData<ChatHistoryPojo>()

    fun chatHistory(doctorId: String) {

        isLoading.value = true
        ChatHistoryRepository.getData({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, doctorId)
    }

}