package com.project.sherby.sherby.data.filter;

public class Search_Result {

    public Integer id;
    public String name;
    public String image;
    public String price;
    public String date;
    public String location;

}