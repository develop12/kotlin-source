package com.project.sherby.sherby.data.receivedMessages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ReceiveMessageHistoryPojo implements Serializable {

    @SerializedName("success")
    @Expose
    public Integer success;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("list")
    @Expose
    public List list;
    @SerializedName("chat")
    @Expose
    public java.util.List<Chat> chat = null;

}