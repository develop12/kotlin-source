package com.project.sherby.sherby.data.pickPackage

import java.io.Serializable

class Package : Serializable {

    var id: String? = null
    var name: String? = null
    var description: String? = null
    var images_number: String? = null
    var activation_period: String? = null
    var raiseup_enabled: String? = null
    var google_map: String? = null
    var price: String? = null


}