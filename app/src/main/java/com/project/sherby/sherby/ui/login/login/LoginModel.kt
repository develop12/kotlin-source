package com.project.sherby.sherby.ui.login.login

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.login.LoginUserPojo
import com.project.sherby.sherby.utils.MyViewModel

/**
 * Created by android on 19/2/18.
 * model class for login
 */
class LoginModel : MyViewModel() {

    var response = MutableLiveData<LoginUserPojo>()

    fun authenticate(user_login: String, user_pass: String, token: String, type: String) {

        isLoading.value = true
        LoginRepository.getData({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, user_login, user_pass, token, type)
    }
}