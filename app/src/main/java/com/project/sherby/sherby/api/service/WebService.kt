package com.project.sherby.sherby.api.service

import com.project.sherby.sherby.data.chatHistory.ChatHistoryPojo
import com.project.sherby.sherby.data.countries.CountriesPojo
import com.project.sherby.sherby.data.favourites.AddFavouritesPojo
import com.project.sherby.sherby.data.favourites.FetchFavouritesPojo
import com.project.sherby.sherby.data.getOtp.GetOtpPojo
import com.project.sherby.sherby.data.home.Category
import com.project.sherby.sherby.data.home.HomeItemsPojo
import com.project.sherby.sherby.data.login.LoginUserPojo
import com.project.sherby.sherby.data.notifications.NotificationPojo
import com.project.sherby.sherby.data.pickPackage.PickPackagePojo
import com.project.sherby.sherby.data.receivedBidMessages.BidPojo
import com.project.sherby.sherby.data.sendMessage.SendMessagePojo
import com.project.sherby.sherby.data.signup.RegisterUserPojo
import com.project.sherby.sherby.data.singlelisting.SingleListingPojo
import com.project.sherby.sherby.data.userProfile.GetUserProfilePojo
import com.project.sherby.sherby.utils.Constants
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface WebService {

    @POST(Constants.Listing)
    fun listing(@Query("Catid") Catid: String,
                @Query("currentPage") currentPage: String): Call<HomeItemsPojo>

    @POST(Constants.GET_OTP)
    @FormUrlEncoded
    fun getOtp(@Field("user_phone") user_phone: String): Call<GetOtpPojo>


    @POST(Constants.FILTER)
    fun filter(@Query("address") address: String,
               @Query("Catid") Catid: String,
               @Query("currentPage") currentPage: String): Call<HomeItemsPojo>

    @POST(Constants.REPORT)
    fun report(@Query("userId") userId: String,
               @Query("postId") postId: String,
               @Query("reason") reason: String,
               @Query("msg") msg: String
    ): Call<RegisterUserPojo>

    //user_login, user_pass,  user_phone,verification_code
    @POST(Constants.REGISTER)
    @FormUrlEncoded
    fun register(@Field("user_login") user_login: String,
                 @Field("user_pass") user_pass: String,
                 @Field("user_phone") user_phone: String,
                 @Field("verification_code") verification_code: String
    ): Call<RegisterUserPojo>

    //package listing

    @POST(Constants.PICK_PACKAGE)
    fun pickPackage(): Call<PickPackagePojo>


//firstName, lastName, nickName, userId, address, email, displayName, password, image

    @POST(Constants.LOGIN)
    @FormUrlEncoded
    fun login(@Field("user_login") user_login: String,
              @Field("user_pass") user_email: String,
              @Field("devicetoken") devicetoken: String,
              @Field("device_type") device_type: String

    ): Call<LoginUserPojo>

    @POST(Constants.SINGLE_LISTING)
    @FormUrlEncoded
    fun singleListing(@Field("postId") postId: String,
                      @Field("userId") userId: String
    ): Call<SingleListingPojo>

    @POST(Constants.CATEGORY)
    fun fetchCategory(): Call<List<Category>>

    @POST(Constants.USER_PROFILE)
    @FormUrlEncoded
    fun userProfile(@Field("userId") userId: String
    ): Call<GetUserProfilePojo>


    @POST(Constants.MAKE_AN_OFFER)
    @FormUrlEncoded
    fun makeOffer(

            @Field("postId") postId: String,
            @Field("bidAmount") bidAmount: String,
            @Field("fromUserId") fromUserId: String,
            @Field("messageContent") messageContent: String

    ): Call<RegisterUserPojo>

//userId, oldPassword, newPassword , http://webappsitesdemo.com/sherby/RestApi/changePassword.php

    @POST(Constants.CHANGE_PASSWORD)
    @FormUrlEncoded
    fun changePassword(
            @Field("userId") userId: String,
            @Field("oldPassword") oldPassword: String,
            @Field("newPassword") newPassword: String
    ): Call<RegisterUserPojo>

    @POST(Constants.FORGOT_PASSWORD)
    @FormUrlEncoded
    fun forgotPassword(
            @Field("user_phone") user_phone: String
    ): Call<RegisterUserPojo>


    @POST(Constants.POST_ITEM)
    @FormUrlEncoded
    fun postItem(/*@PartMap params: Map<String, @JvmSuppressWildcards RequestBody>*/
            @Field("title") title: String,
            @Field("description") description: String,
            @Field("price") price: String,
            @Field("city")    city: String,
            @Field("country") country: String,
            @Field("mobile") mobile: String,
            @Field("cat") cat: String,
            @Field("specification") specification: String,
            @Field("userId") userId: String,
            @Field("images[]") images: ArrayList<String?>,
            @Field("levelId") levelId: String,
            @Field("listingStatus") listingStatus: String
    ): Call<RegisterUserPojo>

    //firstName, lastName, nickName, userId, address, email, displayName, password, image

    @Multipart
    @POST(Constants.EDIT_PROFILE)
    fun editProfile(@PartMap params: Map<String, @JvmSuppressWildcards RequestBody>): Call<RegisterUserPojo>


    @POST(Constants.SEND_MESSAGE)
    @FormUrlEncoded
    fun sendMessage(@Field("postId") postId: String,
                    @Field("messageContent") messageContent: String,
                    @Field("fromUserId") fromUserId: String): Call<SendMessagePojo>


    @POST(Constants.CHAT_HISTORY)
    @FormUrlEncoded
    fun chatHistory(@Field("userId") fromUserId: String): Call<ChatHistoryPojo>


    @POST(Constants.RECEIVED_MESSAGES)
    @FormUrlEncoded
    fun receiveMessages(
            @Field("postId") postId: String,
            @Field("userId") userId: String

    ): Call<BidPojo>


    @POST(Constants.ADD_FAVOURITES)
    @FormUrlEncoded
    fun addFavourites(
            @Field("postId") postId: String,
            @Field("userId") userId: String

    ): Call<AddFavouritesPojo>


    @POST(Constants.FETCH_FAVOURITES)
    @FormUrlEncoded
    fun fetchFavourites(@Field("userId") fromUserId: String): Call<FetchFavouritesPojo>


    @POST(Constants.NOTIFICATION)
    @FormUrlEncoded
    fun notificationTab(@Field("userId") userId: String): Call<NotificationPojo>


//    //user_email,verification_code
//    @POST(Constants.VERIFY)
//    @FormUrlEncoded
//    fun verifyOtp(@Field("user_phone") user_phone: String,
//                  @Field("verification_code") verification_code: String): Call<RegisterUserPojo>


    @POST(Constants.MY_LISTINGS)
    @FormUrlEncoded
    fun fetchMyListings(@Field("userId") fromUserId: String): Call<FetchFavouritesPojo>

    @POST(Constants.FETCH_COUNTRIES)
    fun fetchCountries(): Call<CountriesPojo>


    @POST(Constants.DELETE_POST)
    @FormUrlEncoded
    fun deletePost(
            @Field("post_id") postId: String
    ): Call<AddFavouritesPojo>


    @POST(Constants.PUBLISH_UNPUBLISH)
    @FormUrlEncoded
    fun publishUnpublish(
            @Field("postId") postId: String,
            @Field("status") status: String
    ): Call<AddFavouritesPojo>


    @POST(Constants.SOLD_UNSOLD)
    @FormUrlEncoded
    fun soldUnsold(
            @Field("postId") postId: String,
            @Field("userId") fromUserId: String,
            @Field("status") status: String
    ): Call<AddFavouritesPojo>


    /**
     * rateReview.php
    post
    userId, postId,rating,review
     */

    @POST(Constants.SOLD_UNSOLD)
    @FormUrlEncoded
    fun rateReview(
            @Field("userId") fromUserId: String,
            @Field("postId") postId: String,
            @Field("rating") rating: String,
            @Field("review") review: String
    ): Call<AddFavouritesPojo>

}