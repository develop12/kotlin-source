package com.project.sherby.sherby.ui.userProfile.favourites

import Preferences
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.sherby.sherby.R
import com.project.sherby.sherby.data.favourites.Favourite
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.ui.adsDescription.ViewAdFragment
import com.project.sherby.sherby.ui.home.HomeFragmentModel
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.RecyclerViewClickListener
import kotlinx.android.synthetic.main.app_bar_drawer.*
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * Created by ${Shubham} on 6/25/2018.
 */
class FavouritesListFragment : BaseFragment(), View.OnClickListener, RecyclerViewClickListener {
    private val args = Bundle()
    override fun onClick(view: View, position: Int, url: String) {
        args.putString(Constants.PRODUCT_ID, itemsFavourites[position].id.toString())
        val viewAdFragment = ViewAdFragment()
        viewAdFragment.arguments = args
        activity!!.tvSellYourStuff.visibility = View.INVISIBLE
        addFragment(viewAdFragment, true, R.id.fragment_container)
        activity!!.rlTabLayout.visibility = View.GONE
        activity!!.rlToolbar.visibility = View.GONE
    }

    private var itemsFavourites: ArrayList<Favourite> = ArrayList()
    private var mViewModel: HomeFragmentModel? = null
    override fun onClick(v: View?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(HomeFragmentModel::class.java)
        attachObservers()

    }


    private fun attachObservers() {
        //favourites
        mViewModel?.responseFetchFavourites?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                itemsFavourites = (it.favourites as ArrayList<Favourite>?)!!
                setUpFavouriteRecyclerView()
            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })
    }

    private fun setUpFavouriteRecyclerView() {
        rvHomeItems.layoutManager = StaggeredGridLayoutManager(2, 1)
        //rvHomeItems.adapter = AdapterFavourites(itemsFavourites, requireContext(), this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fetchData()
    }

    private fun fetchData() {
        mViewModel?.fetchFavourites(Preferences.prefs!!.getString(Constants.USER_ID, "o"))
    }

}