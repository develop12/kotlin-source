package com.project.sherby.sherby.data.home

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HomeItemsPojo : Serializable {

    @SerializedName("Listing")
    var listing: ArrayList<Listing>? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("success")
    var success: Int? = null

}
