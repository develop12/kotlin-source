package com.project.sherby.sherby.ui.home

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.favourites.FetchFavouritesPojo
import com.project.sherby.sherby.data.home.HomeItemsPojo
import com.project.sherby.sherby.utils.MyViewModel

class HomeFragmentModel : MyViewModel() {

    var response = MutableLiveData<HomeItemsPojo>()
    var responseLocationFilter = MutableLiveData<HomeItemsPojo>()
    var responseFetchFavourites = MutableLiveData<FetchFavouritesPojo>()

    fun fetchData(/*address: String,*/catid: String, currentPage: String) {

        isLoading.value = true
        HomeFragmentRepository.fetchHomeData({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        },/*address,*/ catid, currentPage)
    }

    fun fetchLocationData(address: String, catid: String, currentPage: String) {

        isLoading.value = true
        HomeFragmentRepository.fetchLocationData({
            responseLocationFilter.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, address, catid, currentPage)

    }

    fun fetchFavourites(userId: String) {

        isLoading.value = true
        HomeFragmentRepository.fetchFavourites({
            responseFetchFavourites.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, userId)
    }

}