package com.project.sherby.sherby.data.userProfile

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class GetUserProfilePojo : Serializable {

    @SerializedName("success")
    @Expose
    var success: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("ProfileInfo")
    @Expose
    var profileInfo: ProfileInfo? = null

}