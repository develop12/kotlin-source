package com.project.sherby.sherby.data.newSingleList

import com.google.gson.annotations.SerializedName


class PostData {

    @SerializedName("authorId")
    var authorId: String? = null
    @SerializedName("bid_placed")
    var bidPlaced: Boolean? = null
    @SerializedName("cat")
    var cat: Any? = null
    @SerializedName("chat_history")
    var chatHistory: Boolean? = null
    @SerializedName("date")
    var date: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("images")
    var images: List<String>? = null
    @SerializedName("inFav")
    var inFav: Boolean? = null
    @SerializedName("latitude")
    var latitude: String? = null
    @SerializedName("location")
    var location: String? = null
    @SerializedName("longitude")
    var longitude: String? = null
    @SerializedName("mobile")
    var mobile: String? = null
    @SerializedName("postId")
    var postId: String? = null
    @SerializedName("price")
    var price: String? = null
    @SerializedName("specification")
    var specification: String? = null
    @SerializedName("sub_cat")
    var subCat: Any? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("userImage")
    var userImage: String? = null
    @SerializedName("userName")
    var userName: String? = null
    @SerializedName("views")
    var views: String? = null

}
/**
 *   http://webappsitesdemo.com/sherby/RestApi/NotificationTab.php
userId
 */