package com.project.sherby.sherby.ui.chats.chatsScreen

import android.arch.lifecycle.MutableLiveData
import com.project.sherby.sherby.data.receivedBidMessages.BidPojo
import com.project.sherby.sherby.data.sendMessage.SendMessagePojo
import com.project.sherby.sherby.utils.MyViewModel


class SendMessageModel : MyViewModel() {

    var response = MutableLiveData<SendMessagePojo>()
    var responseReceiveMessages = MutableLiveData<BidPojo>()

    fun sendMessage(postId: String,
                    messageContent: String,
                    fromUserId: String) {

        isLoading.value = true
        SendReceiveMessageRepository.sendMessage({
            response.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, postId, messageContent, fromUserId)
    }


    //messge history

    fun receiveMessages(
            postId: String, userId: String) {
        isLoading.value = true
        SendReceiveMessageRepository.receiveMessages({
            responseReceiveMessages.value = it
            isLoading.value = false
        }, {
            apiError.value = it
            isLoading.value = false
        }, postId, userId)
    }
}