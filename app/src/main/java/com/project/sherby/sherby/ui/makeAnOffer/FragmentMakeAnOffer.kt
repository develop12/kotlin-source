package com.project.sherby.sherby.ui.makeAnOffer

import Preferences
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.project.sherby.sherby.R
import com.project.sherby.sherby.activity.LauncherActivity
import com.project.sherby.sherby.other.BaseFragment
import com.project.sherby.sherby.utils.Constants
import com.project.sherby.sherby.utils.Validations
import kotlinx.android.synthetic.main.fragment_make_an_offer.*

/**
 * Created by ${Shubham} on 7/23/2018.
 */
class FragmentMakeAnOffer : BaseFragment(), View.OnClickListener {

    private var mViewModel: MakeAnOfferModel? = null
    private var p_id: String = ""


    override fun onClick(v: View?) {
        when (v) {
            tvMakeAnOffer -> {
                if (Validations.isEmpty(etBidAmount)) {
                    if (Validations.isEmpty(etBidMessage)) {
                        mViewModel?.fetchAdsInfo(p_id,
                                etBidAmount.text.toString(),
                                Preferences.prefs!!.getString(Constants.USER_ID, "0"),
                                etBidMessage.text.toString()
                        )
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(MakeAnOfferModel::class.java)
        attachObservers()
    }

    private fun attachObservers() {
        mViewModel?.response?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                if (it.success == 1) {
                    Toast.makeText(activity, "Offer made successfully", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(activity, LauncherActivity::class.java))
                } else
                    showSnackBar(it.message!!)
            }
        })
        mViewModel?.apiError?.observe(this, android.arch.lifecycle.Observer {
            it?.let {
                showSnackBar(it)
            }
        })

        mViewModel?.isLoading?.observe(this, android.arch.lifecycle.Observer {
            it?.let { showLoading(it) }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_make_an_offer, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBundledData()
        setData()
    }

    private fun getBundledData() {
        val bundle = arguments

        if (bundle != null) {
            p_id = bundle.getString(Constants.POST_ID)
        }

    }

    private fun setData() {
        tvMakeAnOffer.setOnClickListener(this)
    }
}